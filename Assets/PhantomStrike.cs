﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhantomStrike : Skills
{
    [Header("ScalingValues")]
    public float[] AttackSpeedBonus;
    public float[] CooldownScaling;
    public float[] ManaCostScaling;
    public override bool Cast(GameObject pCaster, Vector3 targetPos, GameObject target = null)
    {
        if (!base.Cast(pCaster, targetPos, target))
            return false;

        caster.transform.position = target.transform.position;
        caster.transform.position += caster.transform.forward * 2;

        if (target.GetComponent<Unit>().faction != caster.GetComponent<Unit>().faction)
        {
            caster.GetComponent<Attack>().SetTarget(target);
            ApplyEffect(caster);
        }
        return true;
    }
    public override void InitializeEffect(GameObject effect)
    {
        effect.GetComponent<AttackSpeedBuff>().duration = 2;
        effect.GetComponent<AttackSpeedBuff>().power = AttackSpeedBonus[Level - 1];
    }

    public override void LevelUp()
    {
        base.LevelUp();

        Cooldown = CooldownScaling[Level - 1];
        ManaCost = ManaCostScaling[Level - 1];
    }
}
