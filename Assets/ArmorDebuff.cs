﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorDebuff : StatusEffect
{
    public override bool ApplyEffect(Unit target)
    {
        if (!base.ApplyEffect(target))
            return false;

        target.GetComponent<Stats>().armor -= (int)power;

        StartCoroutine(handleTimer());
        return true;
    }

    public override bool RemoveEffect(Unit target)
    {
        if (!base.RemoveEffect(target))
            return false;

        target.GetComponent<Stats>().armor += (int)power;
        return true;
    }
}
