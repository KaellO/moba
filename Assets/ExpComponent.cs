﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpComponent : Resource
{
    PlayerBase player;
    protected override void Start()
    {
        player = GetComponent<PlayerBase>();

        maxValue = player.MaxEXP;

        currentValue = player.EXP;
        calculatedValue = player.EXP;

        player.OnLevelUp += Initialize;
    }

    public override void Initialize()
    {
        maxValue = player.MaxEXP;

        currentValue = player.EXP;
        calculatedValue = player.EXP;

        UpdateResourceUI(currentValue, maxValue);
    }

    public override void ReduceResource(float damage)
    {
        base.ReduceResource(damage);
    }

    public override void AddResource(float value)
    {
        if (player.Level < 25)
        {
            calculatedValue += Mathf.Clamp(value, 0, maxValue);
            currentValue = (int)Mathf.Floor(calculatedValue);
            UpdateResourceUI(currentValue, maxValue);
            player.EXP = currentValue;
        }
    }

    public void LevelUpInstantly()
    {
        AddResource(player.MaxEXP);
    }
}
