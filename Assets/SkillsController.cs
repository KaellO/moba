﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SkillsController : MonoBehaviour
{
    public enum SkillCasted
    {
        QCast,
        WCast,
        ECast,
        RCast
    }
    public static event Action<SkillCasted, Skills> OnSkillCasted;
    public static event Action OnNotEnoughMana;
    public static event Action OnRefreshCD;
    public static event Action<int, Skills, PlayerBase, List<GameObject>> OnSkillLevelUp;
    public static event Action<Skills, int, SkillsController> OnSetUpSkillLevelUI;
    public static event Action<PlayerBase, List<GameObject>> OnSkillPointAcquired;
    public static event Action<int, PlayerBase.HeroType> OnStatsUpgraded;
    public static event Action<SkillsController> OnSetUpStatsUpgrade;

    public GameObject spellSelected;
    SkillCasted spellOrderSelected;
    PlayerBase player;
    Animator animator;
    List<GameObject> skills;

    public bool spellActivated = false;
    private void Start()
    {
        skills = new List<GameObject>();
        player = GetComponent<PlayerBase>();
        animator = GetComponent<Animator>();
        player.OnLevelUp += SkillPointAcquired;

        skills.Add(player.QSkill);
        skills.Add(player.WSkill);
        skills.Add(player.ESkill);
        skills.Add(player.RSkill);

        int skillNumber = 0;

        //Upgrade buttons do not work if the foreach loop is inside SkillsUI instead I have no idea why
        foreach (GameObject skill in skills)
        {
            OnSetUpSkillLevelUI?.Invoke(skill.GetComponent<Skills>(), skillNumber, this);
            skillNumber++;
        }
        OnSetUpStatsUpgrade?.Invoke(this);
    }
    public void RefreshCooldowns()
    {
        foreach (GameObject skill in skills)
        {
            skill.GetComponent<Skills>().StopAllCoroutines();
            skill.GetComponent<Skills>().inCooldown = false;
        }

        OnRefreshCD?.Invoke();
    }

    public void DeactivateSkill()
    {
        spellActivated = false;
        animator.SetBool("turning", true);
        spellSelected.GetComponent<Skills>().DeactivateIndicator();
    }

    public void ActivateSkill(GameObject skill, SkillCasted casted)
    {
        //check if activatable

        Skills skillComp = skill.GetComponent<Skills>();

        //if no targeting required, simply cast
        if (skillComp.targeting == Skills.Targeting.None)
        {
            if (skillComp.Cast(this.gameObject, Vector3.zero, null))
                OnSkillCasted?.Invoke(casted, skillComp);
            return;
        }

        //Activate ability range indicator if not
        if (skillComp.ActivateIndicator(this.gameObject))
        {
            spellSelected = skill;
            spellActivated = true;
            spellOrderSelected = casted;
        }
    }

    public void CastSkill(Skills skillComp, Vector3 targetPos, GameObject target = null)
    {
        if (skillComp.Cast(this.gameObject, targetPos, target))
        {
            //For UI (cooldown)
            OnSkillCasted?.Invoke(spellOrderSelected, skillComp);
            //Actual Cooldown
            StartCoroutine(setCooldown(skillComp));
        }
        else if (GetComponent<ManaComponent>().CurrentValue < skillComp.ManaCost)
        {
            OnNotEnoughMana?.Invoke();
        }
    
    }

    public void UpgradeSkill(int Index, Skills skill)
    {
        skill.gameObject.SetActive(true);
        skill.Unlocked = true;
        if (player.SkillPoints > 0)
        {
            player.SkillPoints--;
            skill.GetComponent<Skills>();
            skill.LevelUp();

            OnSkillLevelUp?.Invoke(Index, skill, player, skills);
        }  
    }

    public void UpgradeStats()
    {
        Debug.Log("Upgrade Stats " + this.gameObject.name);
        if (player.SkillPoints > 0 && player.StatLevelCap > player.StatLevel) 
        {
            player.SkillPoints--;
            player.Str += 2;
            player.Int += 2;
            player.Agi += 2;

            OnStatsUpgraded?.Invoke((int)player.ExStr, PlayerBase.HeroType.StrType);
            OnStatsUpgraded?.Invoke((int)player.ExAgi, PlayerBase.HeroType.AgiType);
            OnStatsUpgraded?.Invoke((int)player.ExInt, PlayerBase.HeroType.IntType);

            player.StatLevel++;

            OnSkillLevelUp?.Invoke(-1, null, player, skills);
        }
    }

    void SkillPointAcquired()
    {
        OnSkillPointAcquired?.Invoke(player, skills);
    }

    IEnumerator setCooldown(Skills skill)
    {
        skill.inCooldown = true;
        yield return new WaitForSeconds(skill.Cooldown);
        skill.inCooldown = false;
    }
}
