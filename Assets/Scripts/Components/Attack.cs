﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Attack : MonoBehaviour
{
    Stats stats;

    public GameObject currentTarget;
    public bool canAttack;

    public float damage;
    public float attackSpeed;
    public float attackRange;
    [SerializeField] SphereCollider attackRangeCollider;

    Animator animator;

    Coroutine currentAttack;
    private void Start()
    {
        animator = GetComponent<Animator>();
        stats = GetComponent<Stats>();

        currentTarget = null;
        canAttack = true;

        damage = stats.damage;
        attackRange = stats.attackRange;

        float attackSpeedAnim = 1 / attackSpeed;

        //animator.SetFloat("attackSpeed", 1);
    }

    public void AutoAttack(GameObject target)
    {
        if (currentTarget == null)
        {
            CancelWaitAttack();
            return;
        }

        if (currentTarget)
            DamageTarget(currentTarget, damage, Skills.DamageType.Physical);

        currentAttack = StartCoroutine(AttackWait());
    }

    public void DamageTarget(GameObject target, float pDamage, Skills.DamageType damageType)
    {
        Stats targetStats = target.GetComponent<Stats>();

        float calculatedDamage = 0;

        switch (damageType)
        {
            case Skills.DamageType.Physical:
                {
                    calculatedDamage = StatModifier.CalculatePhysicalDamage(targetStats.armor, targetStats.armorType, pDamage, stats.damageType);
 
                    float evasion = StatModifier.CalculateEvasion(targetStats.evasion);
                    if (StatModifier.EvasionCheck(evasion))
                    {
                        if (target.TryGetComponent(out FloatingTextSpawner text))
                            text.SpawnText("MISS", Color.green);
                        return;
                    }
                    break;
                }
            case Skills.DamageType.Magic:
                calculatedDamage = pDamage - (pDamage * StatModifier.CalculateMagicMod(targetStats.magicResist, targetStats.magicReduction));
                break;
        }

        float damageMult = StatModifier.CritDamageMultiplier(stats.critSources);
        calculatedDamage *= damageMult;

        if (target.TryGetComponent(out FloatingTextSpawner floatingTextSpawner))
            if (damageMult == 1)
                floatingTextSpawner.SpawnText(calculatedDamage.ToString(), Color.red);
            else
                floatingTextSpawner.SpawnText(calculatedDamage.ToString(), Color.white);

        if (target)
            if (target.TryGetComponent(out HealthComponent healthComp))
                healthComp.ReduceHealth(calculatedDamage, this.gameObject);
    }

    public void SetTarget(GameObject target)
    {
        if (target == null)
        {
            currentTarget = null;
            return;
        }

        currentTarget = target;

        //Remove current target if object gets destroyed.
        currentTarget.GetComponent<HealthComponent>().OnUnitDeath.AddListener((go) => { currentTarget = null; });
    }

    public void CancelWaitAttack()
    {
        canAttack = true;
        StopAllCoroutines();
        //StopCoroutine(currentAttack);
    }

    public void WaitForAttackSpeed()
    {
        currentAttack = StartCoroutine(AttackWait());
    }

    IEnumerator AttackWait()
    {
        canAttack = false;
        yield return new WaitForSeconds(stats.CalculateAttackTime());

        if (animator)
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                animator.Play("Base Layer.Attack", 0, 0);
            }
        canAttack = true;
    }
}
