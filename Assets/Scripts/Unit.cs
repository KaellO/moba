﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Unit : MonoBehaviour
{
    public Action OnRespawn;
    public enum Faction
    {
        Neutral,
        Radiant,
        Dire
    }
    public enum AttackType
    {
        melee,
        ranged
    }
    public bool targettable = true;
    public Faction faction;
    public AttackType attackType;
    public Sprite portrait;

    public new string name;

    public List<StatusEffect> statusEffects;
    protected float lootRange;

    public abstract void Loot(GameObject lootReciever);

    protected virtual void Start()
    {
        HealthComponent health = GetComponent<HealthComponent>();
        health.OnApplyKillReward += Loot;
        lootRange = 13;
    }
}
