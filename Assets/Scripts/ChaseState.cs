﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : UnitBaseFSM
{
    Attack attackScript;
    Movement moveScript;
    Coroutine waitToAttack;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        attackScript = unit.GetComponent<Attack>();
        moveScript = unit.GetComponent<Movement>();
        agent.isStopped = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (attackScript.currentTarget == null)
        {
            animator.SetBool("chasing", false);
            animator.SetBool("idle", true);
        }

        else 
        { 
            Vector3 targetPos = new Vector3(attackScript.currentTarget.transform.position.x,
                                            animator.transform.position.y,
                                            attackScript.currentTarget.transform.position.z);

            moveScript.UpdateLocation(targetPos);

            if (Vector3.Distance(animator.transform.position, targetPos) < (attackScript.attackRange + (attackScript.currentTarget.GetComponent<Collider>().bounds.size.x / 2))
                && attackScript.canAttack)
            {
                animator.SetBool("chasing", false);
                animator.SetTrigger("attacking");
               
            }

            if (moveScript.turnAngle > 11)
            {
                animator.SetBool("chasing", false);
                animator.SetBool("turning", true);
            }
        }     
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("chasing", false);
        animator.ResetTrigger("attacking");
        animator.SetFloat("moveAnimOffset", (stateInfo.normalizedTime + animator.GetFloat("moveAnimOffset")) % 1);
    }
}
