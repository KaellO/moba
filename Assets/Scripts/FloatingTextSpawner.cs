﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextSpawner : MonoBehaviour
{
    Attack attack;
    public GameObject floatingTextPrefab;
    public Transform spawnPosition;

    private void Start()
    {
        attack = GetComponent<Attack>();

        if (!spawnPosition)
            spawnPosition = transform;
    }

    public void SpawnText(string value, Color color)
    {
        GameObject text = Instantiate(floatingTextPrefab, spawnPosition.position, Quaternion.identity);
        text.GetComponent<FloatingTextUI>().Init(value, 100, 5f, color);
    }
}
