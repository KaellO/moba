﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class HealthComponent : Resource
{
    public UnityEvent<GameObject> OnUnitDeath;
    public Action<GameObject> OnApplyKillReward;

    void OnEnable()
    {
        maxValue = stats.maxHealth;

        currentValue = maxValue;
        calculatedValue = currentValue;
        RegenValue = stats.healthRegen;
    }

    protected override void Start()
    {
        base.Start();
        maxValue = stats.maxHealth;

        currentValue = maxValue;
        calculatedValue = currentValue;
        RegenValue = stats.healthRegen;
    }

    public override void Initialize()
    {
        maxValue = stats.maxHealth;

        RegenValue = stats.healthRegen;

        UpdateResourceUI(currentValue, maxValue);
    }

    public void ReduceHealth(float damage, GameObject damageDealer)
    {
        base.ReduceResource(damage);

        if(currentValue <= 0)
        {
            OnUnitDeath?.Invoke(this.gameObject);
            OnApplyKillReward?.Invoke(damageDealer);
            //StartCoroutine(DestroyObject());
        }
    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForEndOfFrame();
        Destroy(this.gameObject);
    }
    public override void AddResource(float value)
    {
        base.AddResource(value);
    }
}
