﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Structure : Unit
{
    public List<GameObject> requiredStructures = new List<GameObject>();
    [SerializeField] int teamGoldReward;
    [SerializeField] int individualGoldReward;
    HealthComponent healthComp;
    protected override void Start()
    {
        base.Start();
        targettable = false;
        healthComp = GetComponent<HealthComponent>();

        foreach(GameObject structure in requiredStructures)
        {
            structure.GetComponent<HealthComponent>().OnUnitDeath.AddListener((go) => requiredStructureDestroyed(go));
        }
        healthComp.OnUnitDeath.AddListener((go) => this.gameObject.SetActive(false));
        
        if (requiredStructures.Count <= 0)
        {
            targettable = true;
        }
    }
    void requiredStructureDestroyed(GameObject structure)
    {
        if (requiredStructures.Contains(structure))
        {
            requiredStructures.Remove(structure);
            if(requiredStructures.Count <= 0)
            {
                targettable = true;
            }
        }
    }
    public override void Loot(GameObject lootReciever)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, lootRange, LayerMask.GetMask("Unit"), QueryTriggerInteraction.Ignore);

        lootReciever.GetComponent<PlayerBase>().Gold += individualGoldReward;
        foreach (GameObject player in PlayerManager.instance.players)
        {
            player.GetComponent<PlayerBase>().Gold += teamGoldReward;
        }
        
    }

    public void MakeTargettable()
    {
        requiredStructures.Clear();
        targettable = true;
    }
}
