﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
using UnityEngine.UI;
using System.Linq;
public class PlayerBase : Unit
{
    public Action OnLevelUp;
    public Action<int> OnGoldRecieved;
    public Action<int> OnResTimerUpdate;
    public Action<int> UpdateDeathCount;
    public Action<int> UpdateKillCount;
    public Action<GameObject> OnHeroKilled;

    Stats stats;
    HealthComponent healthComponent;
    [SerializeField] HeroData heroData;
    [Header("Player Stats")]
    [SerializeField] float strength;
    [SerializeField] float agility;
    [SerializeField] float intelligence;

    int currentEXP = 0;
    int maxEXP;
    int level;
    int maxLevel = 25;
    int baseGoldReward = 110;
    int gold = 600;
    int expReward;

    int kills = 0;
    int deaths = 0;
    int assists = 0;
    [HideInInspector] public int SkillPoints;
    public float PhysicalImmunity;
    public int SkillLevelCap { get; private set; }
    public int UltLevelCap { get; private set; }
    public int StatLevelCap { get; private set; }
    [HideInInspector] public int StatLevel;

    public float ExStr { get; private set; }
    public float ExAgi { get; private set; }
    public float ExInt { get; private set; }
    #region Player Stat Properties
    public float Str
    { get { return strength; }
        set
        {
            ExStr += Mathf.Abs(value - strength);
            strength = Mathf.Clamp(value, 0, float.MaxValue);
            strength = (int)Mathf.Floor(strength);
            stats.maxHealth = (int)Mathf.Floor(strength * 20);
            stats.healthRegen = strength * 0.1f;
            ConvertHeroStats(HeroType.StrType, strength);
        }
    }
    public float Agi
    {
        get { return agility; }
        set
        {
            ExAgi += Mathf.Abs(value - agility);
            stats.AttackSpeed -= agility;
            stats.AttackSpeed += value;

            agility = Mathf.Clamp(value, 0, float.MaxValue);
            agility = (int)Mathf.Floor(agility);
            stats.armor = (int)Mathf.Floor(agility * 0.16f);
            ConvertHeroStats(HeroType.AgiType, agility);
        }
    }
    public float Int
    {
        get { return intelligence; }
        set
        {
            ExInt += Mathf.Abs(value - intelligence);
            intelligence = Mathf.Clamp(value, 0, float.MaxValue);
            intelligence = (int)Mathf.Floor(intelligence);
            stats.maxMana = (int)Mathf.Floor(intelligence * 12);
            stats.manaRegen = 0.05f * intelligence;
            ConvertHeroStats(HeroType.IntType, intelligence);
        }
    }

    public int Gold
    {
        get { return gold; }
        set
        {
            gold = Mathf.Clamp(value, 0, int.MaxValue);
            OnGoldRecieved?.Invoke(gold);
        }
    }

    public int EXP
    {
        get { return currentEXP; }
        set
        {
            currentEXP = Mathf.Clamp(value, 0, int.MaxValue);
            currentEXP = (int)Mathf.Floor(currentEXP);
            if (currentEXP >= maxEXP && level < maxLevel)
                LevelUp();
        }
    }
    public int MaxEXP
    {
        get { return maxEXP; }
    }
    public int Level
    {
        get { return level; }
    }
    #endregion
    public enum HeroType
    {
        StrType,
        AgiType,
        IntType
    }
    public HeroType heroType;

    [Header("Player Skills")]
    public GameObject QSkill;
    public GameObject WSkill;
    public GameObject ESkill;
    public GameObject RSkill;

    public void InitializeHero()
    {
        name = heroData.Name;
        portrait = heroData.HeroPortraitSprite;

        QSkill = heroData.QSkill;
        WSkill = heroData.WSkill;
        ESkill = heroData.ESkill;
        RSkill = heroData.RSkill;

        stats.baseSpeed = heroData.Speed;
        stats.armor = heroData.Armor;
        stats.damage = heroData.Damage;
        stats.baseMagicResist = heroData.MagicRes;
        stats.AttackSpeed = heroData.AttackSpeed;
        stats.attackRange = heroData.AttackRange;

        strength = heroData.Strength;
        agility = heroData.Agility;
        intelligence = heroData.Intelligence;
    }

    private void Awake()
    {
        stats = GetComponent<Stats>();
        heroData = HeroSelectionData.selectedHero;
        InitializeHero();
        maxLevel = 25;
        currentEXP = 0;
        level = 1;

        SkillPoints = 1;
        SkillLevelCap = 1;
        StatLevelCap = 10;
        UltLevelCap = 0;
        StatLevel = 0;
        maxEXP = ScalingValues.heroEXPGrowth[level - 1];
        expReward = ScalingValues.heroExpRewardGrowth[level - 1];
        GetComponent<MinimapIcon>().MinimapIconImage = heroData.minimapIcon;
        Initialize();
    }

    protected override void Start()
    {
        base.Start();
        QSkill = InitializeSkill(QSkill);
        WSkill = InitializeSkill(WSkill);
        ESkill = InitializeSkill(ESkill);
        RSkill = InitializeSkill(RSkill);
        healthComponent = GetComponent<HealthComponent>();
        OnGoldRecieved?.Invoke(gold);
        healthComponent.OnUnitDeath.AddListener(HeroDeath);
    }
    GameObject InitializeSkill(GameObject skill)
    {
        GameObject Skill = Instantiate(skill, this.transform);
        Skill.transform.position = transform.position;
        Skill.SetActive(false);

        return Skill;
    }
    void Initialize()
    {
        Str = strength;
        Agi = agility;
        Int = intelligence;
    }

    void ConvertHeroStats(HeroType type, float value)
    {
        if (heroType == type)
        {
            stats.damage += value;
        }
    }    

    void LevelUp()
    {
        EXP -= MaxEXP;
        level++;
        SkillPoints++;
        expReward = ScalingValues.heroExpRewardGrowth[level - 1];

        Str += heroData.StrGrowth;
        Agi += heroData.AgiGrowth;
        Int += heroData.IntGrowth;

        if (level != maxLevel)
            maxEXP = ScalingValues.heroEXPGrowth[level - 1];
        if (Level % 2 != 0 && SkillLevelCap < 4)
        {
            SkillLevelCap++;
        }
        if (Level % 6 == 0 && UltLevelCap < 3)
        {
            UltLevelCap++;
        }

        OnLevelUp?.Invoke();
    }

    public override void Loot(GameObject lootReciever)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, lootRange, LayerMask.GetMask("Unit"), QueryTriggerInteraction.Ignore);

        int heroCount = hitColliders.Where(hero => hero.CompareTag("Hero") && hero.GetComponent<Unit>().faction != faction).Count();
        int goldReward = baseGoldReward + (Level * 8);

        foreach (Collider col in hitColliders)
        {
            if (col.CompareTag("Hero"))
            {
                PlayerBase player = col.GetComponent<PlayerBase>();
                if (player.faction != faction)
                {
                    if (col.gameObject == lootReciever)
                    {
                        player.Gold += goldReward;

                        if (TryGetComponent(out FloatingTextSpawner textSpawner))
                        {
                            textSpawner.SpawnText(goldReward.ToString(), Color.yellow);
                        }
                        player.OnHeroKilled?.Invoke(player.gameObject);
                        player.kills++;
                        player.UpdateKillCount?.Invoke(player.kills);
                    }
                    col.GetComponent<ExpComponent>().AddResource(expReward / heroCount);
                } 
            }
        }
    }

    void HeroDeath(GameObject hero)
    {
        ActivateHero(false);
        deaths++;
        UpdateDeathCount?.Invoke(deaths);
        StopAllCoroutines();
        StartCoroutine(Respawn());
    }

    void ActivateHero(bool condition)
    {
        GetComponent<CapsuleCollider>().enabled = condition;
        GetComponent<Animator>().enabled = condition;
        GetComponent<Animator>().SetBool("idle", true);
        GetComponent<ManaComponent>().enabled = condition;
        GetComponent<HealthComponent>().enabled = condition;
        GetComponent<ExpComponent>().enabled = condition;
        GetComponent<Movement>().enabled = condition;
        GetComponent<Attack>().enabled = condition;
        if (TryGetComponent(out PlayerController controller))
            controller.enabled = condition;
        //GetComponent<SkillsController>().enabled = condition;
        transform.GetChild(0).gameObject.SetActive(condition);
        transform.GetChild(1).gameObject.SetActive(condition);
        transform.GetChild(2).gameObject.SetActive(condition);
    }

    IEnumerator Respawn()
    {
        int currentTime = ScalingValues.respawnTimer[Level - 1];
        while (currentTime >= 0)
        {
            OnResTimerUpdate?.Invoke(currentTime);
            yield return new WaitForSeconds(1f);
            currentTime--;
        }

        float randX = UnityEngine.Random.Range(PlayerManager.instance.spawnPoints[(int)faction - 1].position.x - 2, PlayerManager.instance.spawnPoints[(int)faction - 1].position.x + 2);
        float randZ = UnityEngine.Random.Range(PlayerManager.instance.spawnPoints[(int)faction - 1].position.z - 2, PlayerManager.instance.spawnPoints[(int)faction - 1].position.z + 2);
        
        //Debug.Log("My position: " + transform.position + "Target position: " + new Vector3(randX, 1, randZ));
        //Set transform when navmesh agent is disabled only since it slides the agent and stops on collision.
        //transform.position = new Vector3(randX, 1, randZ);
        GetComponent<NavMeshAgent>().Warp(new Vector3(randX, 1, randZ));
        ActivateHero(true);
      
        OnRespawn?.Invoke();
    }
}
