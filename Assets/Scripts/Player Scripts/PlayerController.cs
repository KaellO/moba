﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public static event Action<GameObject> OnSelectedUnit;

    [SerializeField] Camera camera;

    public ParticleSystem particle;
    public Animator animator;

    Attack attackComponent;
    Movement movement;
    PlayerBase player;
    GameObject selected;
    SkillsController skillsController;

    bool playerSelected;
    bool loaded = false;
    int unitLayer;

    public RaycastHit rightClickHit;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerBase>();
        animator = GetComponent<Animator>();
        unitLayer = LayerMask.GetMask("Unit");
        attackComponent = GetComponent<Attack>();
        movement = GetComponent<Movement>();
        skillsController = GetComponent<SkillsController>();

        OnSelectedUnit?.Invoke(player.gameObject);
        playerSelected = true;
        loaded = true;
    }

    private void OnEnable()
    {
        if (loaded)
        {
            player = GetComponent<PlayerBase>();
            OnSelectedUnit?.Invoke(player.gameObject);
            playerSelected = true;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!skillsController.spellActivated)
        {
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                //Attack Command
                if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, unitLayer))
                {
                    if (hit.collider.GetComponent<Unit>().faction != player.faction && hit.collider.GetComponent<Unit>().targettable)
                    {
                        attackComponent.SetTarget(hit.collider.gameObject);
                        movement.SetTargetNode(hit.point);

                        ParticleSystem attackIndicator = Instantiate(particle, new Vector3(hit.point.x, 1.5f, hit.point.z), Quaternion.Euler(-90, 0, 0));
                        ParticleSystem.MainModule settings = attackIndicator.GetComponent<ParticleSystem>().main;
                        settings.startColor = new ParticleSystem.MinMaxGradient(Color.red);

                        animator.SetBool("turning", true);
                    }

                }

                //Move Command
                else if (Physics.Raycast(ray.origin, ray.direction, out hit))
                {
                    attackComponent.currentTarget = null;

                    movement.SetTargetNode(hit.point);
                    animator.SetBool("turning", true);
                    Instantiate(particle, new Vector3(hit.point.x, 1.5f, hit.point.z), Quaternion.Euler(-90, 0, 0));
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, unitLayer))
                {
                    OnSelectedUnit?.Invoke(hit.collider.gameObject);
                    playerSelected = false;
                    selected = hit.collider.gameObject;
                }
            }

            SkillController();
        }

        //This is only true if the spell is both activatable and has a target
        else if(skillsController.spellActivated)
        {
            //Choose target / area
            if (Input.GetMouseButtonDown(0))
            {
                Skills skillComp = skillsController.spellSelected.GetComponent<Skills>();
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray.origin, ray.direction, out hit))
                {
                    //if spell casts (proper mana, not in cooldown, update skill UI)
                    skillsController.CastSkill(skillComp, hit.point, hit.collider.gameObject);

                    animator.SetBool("turning", true);
                }
                else
                {
                    skillsController.DeactivateSkill();
                }
            }

            //Cancel
            if (Input.GetMouseButtonDown(1))
            {
                skillsController.DeactivateSkill();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (playerSelected)
            {
                camera.transform.position = new Vector3(player.transform.position.x + 10.5f, camera.transform.position.y, player.transform.position.z);
            }
            else
            {
                playerSelected = true;
                OnSelectedUnit?.Invoke(player.gameObject);
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("idle", true);
        }
    }

    public void SkillController()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            skillsController.ActivateSkill(player.QSkill, SkillsController.SkillCasted.QCast);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            skillsController.ActivateSkill(player.WSkill, SkillsController.SkillCasted.WCast);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            skillsController.ActivateSkill(player.ESkill, SkillsController.SkillCasted.ECast);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            skillsController.ActivateSkill(player.RSkill, SkillsController.SkillCasted.RCast);
        }
    }

    //Debug whatever
    public void DestroySelection()
    {
        if(selected != null)
        {
            selected.GetComponent<HealthComponent>().ReduceResource(99999);
            OnSelectedUnit?.Invoke(player.gameObject);
        }
    }
}
