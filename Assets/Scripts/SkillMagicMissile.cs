﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillMagicMissile : Skills
{
    GameObject projectile;
    [Header("Scaling Values")]
    public float[] DamageScaling;
    public float[] StunScaling;
    public float[] CooldownScaling;
    public float[] ManaCostScaling;

    protected override void Start()
    {
        base.Start();
        inCooldown = false;
    }

    public override bool Cast(GameObject pCaster, Vector3 targetPos, GameObject target = null)
    {
        if(!base.Cast(pCaster, targetPos, target))
            return false;

        shootProjectile.altTarget = target;
        shootProjectile.shootPoint = caster.transform;
        shootProjectile.shootPoint.position = new Vector3(caster.transform.position.x, caster.transform.position.y + 3, caster.transform.position.z);
        shootProjectile.attackComp = pCaster.GetComponent<Attack>();

        projectile = shootProjectile.SingleTargetProjectile();
        Projectile projectileComp = projectile.GetComponent<Projectile>();

        projectileComp.OnHit += OnProjectileHit;
        return true;
    }

    protected override void OnProjectileHit(GameObject hit)
    {
        if (CanAffect(hit))
        {
            ApplyEffect(hit);

            if (caster && hit)
                caster.GetComponent<Attack>().DamageTarget(hit, Damage, damageType);

            Destroy(projectile);
        }
    }

    public override void InitializeEffect(GameObject effect)
    {
        effect.GetComponent<StunEffect>().duration = StunScaling[Level - 1];
    }

    public override void LevelUp()
    {
        base.LevelUp();
        Damage = DamageScaling[Level - 1];
        Cooldown = CooldownScaling[Level - 1];
        ManaCost = ManaCostScaling[Level - 1];
    }
}
