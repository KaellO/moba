﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectionUI : MonoBehaviour
{
    public Image Portrait;
    public TextMeshProUGUI name;
    public HealthBarUI healthBar;
    public HealthBarUI manaBar;

    [Header("Stats")]
    public TextMeshProUGUI damage;
    public TextMeshProUGUI armor;
    public TextMeshProUGUI speed;
    public GameObject heroStatsPanel;
    public TextMeshProUGUI strength;
    public TextMeshProUGUI agility;
    public TextMeshProUGUI intelligence;
    public TextMeshProUGUI exStr;
    public TextMeshProUGUI exAgi;
    public TextMeshProUGUI exInt;

    [Header("Full Stats")]
    public GameObject FullStatsPanel;
    public TextMeshProUGUI AttackSpeed;
    public TextMeshProUGUI Damage;
    public TextMeshProUGUI AttackRange;
    public TextMeshProUGUI MoveSpeed;
    public TextMeshProUGUI ManaRegen;
    public TextMeshProUGUI Armor;
    public TextMeshProUGUI MagicResist;
    public TextMeshProUGUI HealthRegen;

    [Header("Hero Stats")]
    public GameObject strPanel;
    public GameObject agiPanel;
    public GameObject intPanel;

    public TextMeshProUGUI fStrength;
    public TextMeshProUGUI fAgility;
    public TextMeshProUGUI fIntelligence;

    [Header("EXP Bar")]
    public TextMeshProUGUI levelText;
    public HealthBarUI EXPBar;

    public TextMeshProUGUI respawnText;

    Stats stats;
    Unit unitData;
    GameObject selectedUnit;
    PlayerBase pBase;
    private void Awake()
    {
        PlayerController.OnSelectedUnit += DisplaySelectedUnit;
        SkillsController.OnStatsUpgraded += UpdateStatUpgradeText;
    }

    public void DisplaySelectedUnit(GameObject SelectedUnit)
    {
        if (pBase)
        {
            pBase.OnLevelUp -= UpdateStats;
            pBase.OnResTimerUpdate -= SetRespawnTimer;
            pBase.OnRespawn -= Respawned;
        }
        selectedUnit = SelectedUnit;
        strPanel.GetComponent<Image>().enabled = false;
        agiPanel.GetComponent<Image>().enabled = false;
        intPanel.GetComponent<Image>().enabled = false;

        unitData = selectedUnit.GetComponent<Unit>();
        stats = selectedUnit.GetComponent<Stats>();

        Portrait.sprite = unitData.portrait;
        name.text = unitData.name;

        heroStatsPanel.SetActive(selectedUnit.TryGetComponent(out pBase));
        if(pBase)
        {
            pBase.OnLevelUp += UpdateStats;
            pBase.OnResTimerUpdate += SetRespawnTimer;
            pBase.OnRespawn += Respawned;
        }

        UpdateStats();

        healthBar.ChangeHealthComponent(selectedUnit.GetComponent<HealthComponent>());
        manaBar.ChangeHealthComponent(selectedUnit.GetComponent<ManaComponent>());
        EXPBar.ChangeHealthComponent(selectedUnit.GetComponent<ExpComponent>());
    }

    public void SetRespawnTimer(int text)
    {
        respawnText.gameObject.SetActive(true);
        respawnText.text = text.ToString();
        Portrait.color = new Color(0.2f, 0.2f, 0.2f);
    }

    public void Respawned()
    {
        respawnText.gameObject.SetActive(false);
        Portrait.color = Color.white;
    }
    void UpdateStats()
    {
        damage.text = "Atk: " + stats.damage.ToString();
        armor.text = "Def: " + stats.armor.ToString();
        speed.text = "Spd: " + stats.Speed.ToString();

        strPanel.SetActive(pBase);
        agiPanel.SetActive(pBase);
        intPanel.SetActive(pBase);
        if (pBase)
        {
            levelText.text = pBase.Level.ToString();
            strength.text = "Str: " + pBase.Str.ToString();
            intelligence.text = "Int: " + pBase.Int.ToString();
            agility.text = "Agi: " + pBase.Agi.ToString();

            fStrength.text = pBase.Str.ToString();
            fAgility.text = pBase.Agi.ToString();
            fIntelligence.text = pBase.Int.ToString();

            switch (pBase.heroType)
            {
                case PlayerBase.HeroType.StrType:
                    strPanel.GetComponent<Image>().enabled = true;
                    break;
                case PlayerBase.HeroType.AgiType:
                    agiPanel.GetComponent<Image>().enabled = true;
                    break;
                case PlayerBase.HeroType.IntType:
                    intPanel.GetComponent<Image>().enabled = true;
                    break;
            }
        }

        //Full Stats
        AttackSpeed.text = stats.AttackSpeed.ToString();
        Damage.text = stats.damage.ToString();
        AttackRange.text = stats.attackRange.ToString();
        MoveSpeed.text = stats.Speed.ToString();
        ManaRegen.text = stats.manaRegen.ToString();
        Armor.text = stats.armor.ToString();
        MagicResist.text = stats.baseMagicResist.ToString();
        HealthRegen.text = stats.healthRegen.ToString();
    }

    public void UpdateStatUpgradeText(int playerUpgrade, PlayerBase.HeroType type)
    {
        switch (type)
        {
            case PlayerBase.HeroType.StrType:
                exStr.text = "+" + playerUpgrade.ToString();
                break;
            case PlayerBase.HeroType.AgiType:
                exAgi.text = "+" + playerUpgrade.ToString();
                break;
            case PlayerBase.HeroType.IntType:
                exInt.text = "+" + playerUpgrade.ToString();
                break;
        }
    }

    private void OnDisable()
    {
        PlayerController.OnSelectedUnit -= DisplaySelectedUnit;
    }

    public void OnPortraitHover()
    {
        FullStatsPanel.SetActive(true);
    }

    public void OnPortraitExit()
    {
        FullStatsPanel.SetActive(false);
    }
}
