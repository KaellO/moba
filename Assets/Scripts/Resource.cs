﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Resource : MonoBehaviour
{
    protected Stats stats;
    public event Action<float, float> OnUpdateResource;

    public int maxValue;
    protected int currentValue;
    public int CurrentValue
    {
        get { return Mathf.Clamp(currentValue, 0, maxValue); }
    }
    protected float calculatedValue;
    public float RegenValue { get; protected set; }
    protected bool regening;

    protected void Awake()
    {
        stats = GetComponent<Stats>();
    }

    protected virtual void Start()
    {
        if (TryGetComponent(out PlayerBase player))
        {
            player.OnLevelUp += Initialize;
        }
    }

    public virtual void Initialize()
    {
        OnUpdateResource?.Invoke(currentValue, maxValue);
    }

    protected void UpdateResourceUI(float res1, float res2)
    {
        OnUpdateResource?.Invoke(res1, res2);
    }

    public virtual void ReduceResource(float damage)
    {
        calculatedValue -= Mathf.Clamp(damage, 0, maxValue);
        currentValue = (int)Mathf.Floor(calculatedValue);
        currentValue = Mathf.Clamp(currentValue, 0, maxValue);
        OnUpdateResource?.Invoke((float)currentValue, (float)maxValue);
    }

    public virtual void AddResource(float value)
    {
        calculatedValue += Mathf.Clamp(value, 0, maxValue);
        currentValue = (int)Mathf.Floor(calculatedValue);
        currentValue = Mathf.Clamp(currentValue, 0, maxValue);
        OnUpdateResource?.Invoke((float)currentValue, (float)maxValue);
    }

    protected virtual void Update()
    {
        if (currentValue != maxValue && !regening && RegenValue > 0)
        {
            StartCoroutine(regenHealthOverTime());
        }
    }

    protected IEnumerator regenHealthOverTime()
    {
        regening = true;
        while (currentValue != maxValue)
        {
            AddResource(RegenValue);
            yield return new WaitForSeconds(1f);
        }
        regening = false;
    }
}
