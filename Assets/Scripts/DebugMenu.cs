﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugMenu : MonoBehaviour
{
    public Text message;
    public GameObject menu;

    private float timeScale = 1;
    private CanvasGroup cg;

    private void Awake()
    {
        cg = GetComponent<CanvasGroup>();
    }

    // Start is called before the first frame update
    void Start()
    {
        ChangeMessage("");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            ToggleMenu(); 
    }

    public void ToggleMenu()
    {
        if (cg.alpha == 1)
        {
            cg.interactable = false;
            cg.alpha = 0;
        } else
        {
            cg.interactable = true;
            cg.alpha = 1;
        }

    }

    public void ChangeMessage(string text)
    {
        message.text = string.Concat("", text);
    }

    public IEnumerator NotifyPlayer(string text)
    {
        ChangeMessage(text);
        yield return new WaitForSecondsRealtime(2);
        ChangeMessage("");

    }

    public void DecreaseGameSpeed()
    {
        Time.timeScale *= 0.5f;
        timeScale = Time.timeScale;
        StartCoroutine(NotifyPlayer("Speed set to " + timeScale + "x"));
    }

    public void IncreaseGameSpeed()
    {
        Time.timeScale *= 2.0f;
        timeScale = Time.timeScale;
        StartCoroutine(NotifyPlayer("Speed set to " + timeScale + "x"));
    }
}
