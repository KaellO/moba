﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TurnState : UnitBaseFSM
{
    Movement movement;
    Attack attackScript;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        movement = unit.GetComponent<Movement>();
        attackScript = unit.GetComponent<Attack>();
        agent.isStopped = true;
        attackScript.CancelWaitAttack();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (movement.turnAngle > 11)
        {
            Vector3 cornerDirection = Vector3.RotateTowards(unit.transform.forward, movement.targetDir, Time.deltaTime * movement.rotationSpeed, 0.0f);
            unit.transform.rotation = Quaternion.LookRotation(cornerDirection);
        }

        if (movement.turnAngle < 11)
        {
            if (attackScript.currentTarget != null)
            {
                animator.SetBool("chasing", true);
                animator.SetBool("turning", false);
            }
            else
            {
                animator.SetBool("moving", true);
                animator.SetBool("turning", false);
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("turning", false);

        //This blends in movement and turning smoothly
        animator.SetFloat("moveAnimOffset", (stateInfo.normalizedTime + animator.GetFloat("moveAnimOffset")) % 1);
    }
}
