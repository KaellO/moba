﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : UnitBaseFSM
{
    Attack attackScript;
    GameObject target;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        attackScript = unit.GetComponent<Attack>();
        target = attackScript.currentTarget;

        agent.isStopped = true;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (attackScript.currentTarget == null)
        {
            animator.ResetTrigger("attacking");
            animator.SetBool("idle", true);
            return;
        }

        Vector3 targetPos = new Vector3(attackScript.currentTarget.transform.position.x,
                                animator.transform.position.y,
                                attackScript.currentTarget.transform.position.z);

        if (Vector3.Distance(animator.transform.position, targetPos) > (((unit.GetComponent<Collider>().bounds.size.x / 2) + attackScript.attackRange) + 
            (attackScript.currentTarget.GetComponent<Collider>().bounds.size.x / 2)))
        {
            animator.SetBool("turning", true);
            animator.ResetTrigger("attacking");
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("attacking");
    }
}
