﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveState : UnitBaseFSM
{
    Movement movement;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        movement = unit.GetComponent<Movement>();
        agent.isStopped = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0)
        {
            animator.SetBool("idle", true);
        }

        if (movement.turnAngle > 11)
        {
            animator.SetBool("turning", true);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("moving", false);
        animator.SetFloat("moveAnimOffset", (stateInfo.normalizedTime + animator.GetFloat("moveAnimOffset")) % 1);
    }
}
