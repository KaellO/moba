﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitBaseFSM : StateMachineBehaviour
{
    public GameObject unit;
    public NavMeshAgent agent;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        unit = animator.gameObject;
        agent = unit.GetComponent<NavMeshAgent>();
    }
}
