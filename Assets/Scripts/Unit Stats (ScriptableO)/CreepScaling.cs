﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CreepScaling Data", menuName = "CreepScalingData")]
public class CreepScaling : ScriptableObject
{
    public int HealthGrowth;
    public int AttackDamageGrowth;
    public int GoldBountyGrowth;
    public int ExperienceBountyGrowth;
}
