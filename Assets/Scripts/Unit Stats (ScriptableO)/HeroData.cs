﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Hero Data", menuName = "HeroData")]
public class HeroData : ScriptableObject
{
    [Header("Basic Details")]
    public string Name;
    public Sprite HeroPortraitSprite;
    public GameObject minimapIcon;

    [Header("Hero Skills")]
    public GameObject QSkill;
    public GameObject WSkill;
    public GameObject ESkill;
    public GameObject RSkill;

    [Header("Basic Stats")]
    public float Speed;
    public int Armor;
    public float Damage;
    public float MagicRes;
    public float AttackSpeed;
    public float AttackRange;

    [Header("Hero Base Stats")]
    public float Strength;
    public float Agility;
    public float Intelligence;

    [Header("Stat Growth")]
    public float StrGrowth;
    public float IntGrowth;
    public float AgiGrowth;
}
