﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FloatingTextUI : MonoBehaviour
{
    Vector3 targetTransform;
    TextMeshPro text;

    float moveSpeed = 100;
    float fadeSpeed = 1;

    private void Awake()
    {
        text = GetComponent<TextMeshPro>();
    }
    void Start()
    {
        targetTransform = new Vector3(transform.position.x, transform.position.y + 5,
            transform.position.z);
    }

    public void Init(string value, float moveSpeed, float fadeSpeed, Color pcolor)
    {
        text.text = value;
        text.color = pcolor;

        StartCoroutine(MoveText());
        StartCoroutine(FadeText());
    }

    IEnumerator MoveText()
    {
        float timeOfTravel = moveSpeed;
        float currentTime = 0;
        float normalizedValue;

        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel;

            transform.position = Vector3.Lerp(transform.position, targetTransform, normalizedValue);
            yield return null;
        }
    }

    IEnumerator FadeText()
    {
        float speed = fadeSpeed;

        Color32 textColor = text.faceColor;

        while (text.color.a >= 0)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - (Time.deltaTime * speed));
            yield return null;
        }

        DestroyObject();
    }

    private void Update()
    {
        Vector3 lookDir = transform.position - Camera.main.gameObject.transform.position;
        lookDir.z = 0;

        transform.rotation = Quaternion.LookRotation(lookDir, Vector3.up);
    }

    void DestroyObject()
    {
        StopAllCoroutines();
        Destroy(this.gameObject);
    }
}
