﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class AutoTargetComponent : MonoBehaviour
{
    Unit unitData;

    public List<GameObject> targets = new List<GameObject>();
    public event Action<GameObject> OnTargetFound;
    public event Action<GameObject> OnFirstTargetFound;
    public event Action<GameObject> OnTargetLeft;

    public Unit.Faction targetFaction;

    private void Start()
    {
        unitData = GetComponentInParent<Unit>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Unit") && !targets.Contains(other.gameObject))
        {
            Unit unit = other.GetComponent<Unit>();
            if (!unit)
            {
                Debug.Log(other.name + " not a unit but is in unit layer");
                return;
            }
            if (unit.faction == targetFaction && unit.targettable)
            {
                targets.Add(other.gameObject);
                if (targets.Count == 1 )
                {
                    OnFirstTargetFound?.Invoke(other.gameObject);
                }
                OnTargetFound?.Invoke(other.gameObject);
                other.GetComponent<HealthComponent>().OnUnitDeath?.AddListener((go) => { targets.Remove(go /*other.gameObject*/); });
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (targets.Count > 0 && targets.Contains(other.gameObject))
        {
            targets.Remove(other.gameObject);
            OnTargetLeft?.Invoke(other.gameObject);
        }
    }
}
