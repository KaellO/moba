﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkillsUI : MonoBehaviour
{
    public Image QSkillIcon;
    public Image QSkillCD;
    List<GameObject> QSkillLevel = new List<GameObject>();
    public GameObject QSkillLevelPanel;

    public Image WSkillIcon;
    public Image WSkillCD;
    List<GameObject> WSkillLevel = new List<GameObject>();
    public GameObject WSkillLevelPanel;

    public Image ESkillIcon;
    public Image ESkillCD;
    List<GameObject> ESkillLevel = new List<GameObject>();
    public GameObject ESkillLevelPanel;

    public Image RSkillIcon;
    public Image RSkillCD;
    List<GameObject> RSkillLevel = new List<GameObject>();
    public GameObject RSkillLevelPanel;

    public List<GameObject> SkillUpgradeButtons;
    public GameObject StatsUpgradeButton;
    List<List<GameObject>> SkillLevels = new List<List<GameObject>>();
    List<GameObject> levelUpPanels;
    List<Image> coolDownIcons = new List<Image>();
    [SerializeField] List<Image> skillIcons = new List<Image>();
    public GameObject SkillUpgradePrefab;

    public GameObject NoManaAlertText;
    private void Start()
    {
        SkillUpgradeButtons[SkillUpgradeButtons.Count - 1].SetActive(false);

        levelUpPanels = new List<GameObject>();
        levelUpPanels.Add(QSkillLevelPanel);
        levelUpPanels.Add(WSkillLevelPanel);
        levelUpPanels.Add(ESkillLevelPanel);
        levelUpPanels.Add(RSkillLevelPanel);
        SkillLevels.Add(QSkillLevel);
        SkillLevels.Add(WSkillLevel);
        SkillLevels.Add(ESkillLevel);
        SkillLevels.Add(RSkillLevel);
        coolDownIcons.Add(QSkillCD);
        coolDownIcons.Add(WSkillCD);
        coolDownIcons.Add(ESkillCD);
        coolDownIcons.Add(RSkillCD);

        SkillsController.OnNotEnoughMana += CallNotEnoughManaCoroutine;
        SkillsController.OnSkillCasted += UpdateCooldown;
        SkillsController.OnRefreshCD += RefreshCooldowns;
        SkillsController.OnSkillLevelUp += LevelUpSkills;
        SkillsController.OnSetUpSkillLevelUI += SetupSkills;
        SkillsController.OnSkillPointAcquired += SetSkillButtonUpgrade;
        SkillsController.OnSetUpStatsUpgrade += SetupStats;
    }

    void SetupSkills(Skills skill, int skillNum, SkillsController skillsController)
    {
        for (int i = 0; i < skill.MaxLevel; i++)
        {
            SkillLevels[skillNum].Add(Instantiate(SkillUpgradePrefab, levelUpPanels[skillNum].transform));
        }
        
        SkillUpgradeButtons[skillNum].GetComponent<Button>().onClick.AddListener(delegate { skillsController.UpgradeSkill(skillNum, skill); });
        skillIcons[skillNum].sprite = skill.skillIcon;
        StatsUpgradeButton.SetActive(false);
    }

    void SetupStats(SkillsController skillsController)
    {
        StatsUpgradeButton.GetComponent<Button>().onClick.AddListener(delegate { skillsController.UpgradeStats(); });
    }

    public void LevelUpSkills(int index, Skills skill, PlayerBase player, List<GameObject> skills)
    {
        if (index != -1)
        {
            SkillLevels[index][skill.Level - 1].GetComponent<Image>().color = new Color(1, 0.8f, 0.016f);
            coolDownIcons[index].fillAmount = 0;
            if (skill.Level >= skill.MaxLevel)
            {
                SkillUpgradeButtons[index].SetActive(false);
                
                return;
            }
        }

        if (player.SkillPoints <= 0)
        {
            foreach (GameObject button in SkillUpgradeButtons)
            {
                button.SetActive(false);
            }
            StatsUpgradeButton.SetActive(false);
        }
        else
        {
            SetSkillButtonUpgrade(player, skills);
        }
    }
    public void SetSkillButtonUpgrade(PlayerBase player, List<GameObject> skill)
    {
        if (player.StatLevelCap > player.StatLevel)
            StatsUpgradeButton.SetActive(true);
        else
            StatsUpgradeButton.SetActive(false);

        for (int i = 0; i < SkillUpgradeButtons.Count-1; i++)
        {
            if (skill[i].GetComponent<Skills>().Level < player.SkillLevelCap)
                SkillUpgradeButtons[i].SetActive(true);
            else
                SkillUpgradeButtons[i].SetActive(false);
        }

        if (skill[SkillUpgradeButtons.Count - 1].GetComponent<Skills>().Level < player.UltLevelCap)
            SkillUpgradeButtons[SkillUpgradeButtons.Count - 1].SetActive(true);
        else
            SkillUpgradeButtons[SkillUpgradeButtons.Count - 1].SetActive(false);
    }

    public void UpdateCooldown(SkillsController.SkillCasted skillCasted, Skills skill)
    {
        switch (skillCasted)
        {
            case SkillsController.SkillCasted.QCast:
                StartCoroutine(setCooldown(QSkillCD, skill.Cooldown));
                break;
            case SkillsController.SkillCasted.WCast:
                StartCoroutine(setCooldown(WSkillCD, skill.Cooldown));
                break;
            case SkillsController.SkillCasted.ECast:
                StartCoroutine(setCooldown(ESkillCD, skill.Cooldown));
                break;
            case SkillsController.SkillCasted.RCast:
                StartCoroutine(setCooldown(RSkillCD, skill.Cooldown));
                break;
        }
    }

    public void RefreshCooldowns()
    {
        StopAllCoroutines();
        QSkillCD.fillAmount = 0;
        WSkillCD.fillAmount = 0;
        ESkillCD.fillAmount = 0;
        RSkillCD.fillAmount = 0;
        NoManaAlertText.SetActive(false);
    }

    void CallNotEnoughManaCoroutine()
    {
        StartCoroutine(NotEnoughMana());
    }
    IEnumerator NotEnoughMana()
    {
        NoManaAlertText.SetActive(true);
        yield return new WaitForSeconds(2f);
        NoManaAlertText.SetActive(false);
    }

    IEnumerator setCooldown(Image CDImage, float cooldown)
    {
        float currentDuration = 0;
        CDImage.fillAmount = 1;

        while (CDImage.fillAmount > 0)
        {
            currentDuration += Time.deltaTime;
            CDImage.fillAmount = 1 - (currentDuration / cooldown);
            yield return null;
        }
    }

    private void OnDisable()
    {
        SkillsController.OnNotEnoughMana -= CallNotEnoughManaCoroutine;
        SkillsController.OnSkillCasted -= UpdateCooldown;
        SkillsController.OnRefreshCD -= RefreshCooldowns;
        SkillsController.OnSkillLevelUp -= LevelUpSkills;
        SkillsController.OnSetUpSkillLevelUI -= SetupSkills;
        SkillsController.OnSkillPointAcquired -= SetSkillButtonUpgrade;
        SkillsController.OnSetUpStatsUpgrade -= SetupStats;
    }
}
