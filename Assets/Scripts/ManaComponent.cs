﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaComponent : Resource
{
    private void OnEnable()
    {
        maxValue = stats.maxMana;

        currentValue = maxValue;
        calculatedValue = currentValue;
        RegenValue = stats.manaRegen;
    }
    protected override void Start()
    {
        base.Start();
        maxValue = stats.maxMana;

        currentValue = maxValue;
        calculatedValue = currentValue;
        RegenValue = stats.manaRegen;
    }


    public override void Initialize()
    {
        maxValue = stats.maxMana;

        RegenValue = stats.manaRegen;

        UpdateResourceUI(currentValue, maxValue);
    }
    public override void ReduceResource(float damage)
    {
        base.ReduceResource(damage);
    }

    public override void AddResource(float value)
    {
        base.AddResource(value);
    }
}
