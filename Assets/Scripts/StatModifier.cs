﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class StatModifier
{
    public static float CalculatePhysicalDamage(float armor, Stats.ArmorType armorType, float damage, Stats.DamageType damageType)
    {
        float calculatedDamage = 1 - ((0.052f * armor) / (0.9f + 0.048f * Mathf.Abs(armor)));
        float finalDamage = damage * calculatedDamage;

        float damageMult = 1;
        switch(damageType)
        {
            case Stats.DamageType.Basic:
                switch (armorType)
                {
                    case Stats.ArmorType.Basic:
                        damageMult = 1;
                        break;
                    case Stats.ArmorType.Fortified:
                        damageMult = 0.7f;
                        break;
                    case Stats.ArmorType.Hero:
                        damageMult = 0.75f;
                        break;
                }
                break;
            case Stats.DamageType.Pierce:
                switch (armorType)
                {
                    case Stats.ArmorType.Basic:
                        damageMult = 1.5f;
                        break;
                    case Stats.ArmorType.Fortified:
                        damageMult = 0.35f;
                        break;
                    case Stats.ArmorType.Hero:
                        damageMult = 0.5f;
                        break;
                }
                break;
            case Stats.DamageType.Siege:
                switch (armorType)
                {
                    case Stats.ArmorType.Basic:
                        damageMult = 1;
                        break;
                    case Stats.ArmorType.Fortified:
                        damageMult = 2.5f;
                        break;
                    case Stats.ArmorType.Hero:
                        damageMult = 1f;
                        break;
                }
                break;
            case Stats.DamageType.Hero:
                switch (armorType)
                {
                    case Stats.ArmorType.Basic:
                        damageMult = 1;
                        break;
                    case Stats.ArmorType.Fortified:
                        damageMult = 0.5f;
                        break;
                    case Stats.ArmorType.Hero:
                        damageMult = 1;
                        break;
                }
                break;
        }

        finalDamage *= damageMult;

        Mathf.Clamp(finalDamage, 0, float.MaxValue);

        return Mathf.Ceil(finalDamage);
    }

    //Use IEnumerable
    public static float CalculateMagicMod(List<float> mResist, List<float> mReduction)
    {
        float totalResistanceMod = 1;
        float totalReductionMod = 1;

        foreach (float res in mResist)
            totalResistanceMod *= 1 - res;

        foreach (float red in mReduction)
            totalReductionMod *= 1 + red;

        float finalResistance = 1 - (totalResistanceMod * totalReductionMod);

        //applied in attack stat instead
        float damageModFromMagicResistance = 1 - finalResistance;

        return Mathf.Clamp(finalResistance, float.MinValue, 1);
    }

    public static float CalculateSpeedMod(List<float> slows, List<float> bonusSpeeds, float flatMod, float baseSpeed)
    {
        float totalSpeed = 3f;
        float bonusSpeedTotal = 0;

        foreach (float res in slows)
            totalSpeed *= 1 - res;

        foreach (float bonus in bonusSpeeds)
            bonusSpeedTotal += bonus;

        totalSpeed *= (1 + bonusSpeedTotal);

        totalSpeed *= baseSpeed + flatMod;

        return Mathf.Clamp(totalSpeed, 3f, 5.5f);
    }

    public static float CalculateEvasion(Dictionary<string, float> evasionMod)
    {
        float evasion = 1;

        foreach(KeyValuePair<string, float> entry in evasionMod)
        {
            evasion *= 1 - entry.Value;
        }


        return 1 - evasion;
    }

    public static bool EvasionCheck(float evasion)
    {
        float rng = Random.Range(0, 100);
        if (evasion < rng)
            return false;
        else
            return true;
    }

    public static float CritDamageMultiplier(List<CriticalStrikeSource> critSource)
    {
        List<CriticalStrikeSource> successSource = new List<CriticalStrikeSource>();

        foreach (CriticalStrikeSource source in critSource)
        {
            float rng = Random.Range(0, 100);
            if (source.CriticalRate > rng)
                successSource.Add(source);
        }


        if (successSource.Count == 0)
            return 1;
        else
        {
            return successSource.Max(s => s.CriticalDamage);
        }
    }
}
