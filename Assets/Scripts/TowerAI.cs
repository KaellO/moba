﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAI : MonoBehaviour
{
    public AutoTargetComponent EnemyDetection;
    public AutoTargetComponent AuraDetection;

    public StatusEffect buffEffect;
    public float defenceBuffPower;

    ShootProjectile projectileComp;
    Attack attackComp;
    Stats stats;

    bool shooting = false;

    private void Start()
    {
        EnemyDetection.OnFirstTargetFound += EnemyDetected;
        EnemyDetection.OnTargetLeft += EnemyLeft;

        AuraDetection.OnTargetFound += ApplyBuff;
        AuraDetection.OnTargetLeft += RemoveBuff;

        projectileComp = GetComponent<ShootProjectile>();
        attackComp = GetComponent<Attack>();
        stats = GetComponent<Stats>();
    }

    void EnemyDetected(GameObject enemy)
    {
        attackComp.SetTarget(enemy);

        if(!shooting)
            StartCoroutine(Shoot());
    }

    void EnemyLeft(GameObject enemy)
    {
        attackComp.SetTarget(null);
    }

    void ApplyBuff(GameObject ally)
    {
        if(ally.CompareTag("Hero"))
        {
            //add to buff API
            if (!ally.GetComponent<Unit>().statusEffects.Contains(buffEffect))
            {
                buffEffect.power = defenceBuffPower;
                buffEffect.ApplyEffect(ally.GetComponent<Unit>());
            }
        }
    }

    void RemoveBuff(GameObject ally)
    {
        if (ally.CompareTag("Hero"))
        {
            if (ally.GetComponent<Unit>().statusEffects.Contains(buffEffect))
            {
                buffEffect.power = defenceBuffPower;
                buffEffect.RemoveEffect(ally.GetComponent<Unit>());
            }
        }
    }

    IEnumerator Shoot()
    {
        shooting = true;

        while (attackComp.currentTarget)
        {
            projectileComp.SingleTargetProjectile();
            yield return new WaitForSeconds(stats.AttackSpeed);
        }

        shooting = false;
    }
}
