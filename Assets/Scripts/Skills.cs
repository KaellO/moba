﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skills : MonoBehaviour
{
    public enum Targeting
    {
        None,
        Self,
        Single,
        GroundSphere,
        Ground,
        Global
    }
    public enum Affects
    {
        Self,
        Enemy,
        EnemyHero,
        Ally,
        AllyHero
    }
    public enum DamageType
    {
        Physical,
        Magic
    }
    public GameObject caster;
    [Header("Skill Data")]
    [SerializeField] GameObject rangeIndicator;
    public Sprite skillIcon;
    [SerializeField] protected bool Activatable;
    public Targeting targeting; 
    [SerializeField] protected List<Affects> affects;
    public DamageType damageType;
    
    //Damage Type
    [SerializeField] protected float CastRange;
    [SerializeField] protected float Damage;
    public float Cooldown;
    public float ManaCost;
    public bool inCooldown = false;
    public bool Unlocked = false;
    GameObject target;

    [SerializeField] protected ShootProjectile shootProjectile;
    [SerializeField] protected List<GameObject> effects;

    GameObject currentRangeIndicator;

    public int Level { get; protected set; }
    public int MaxLevel { get; protected set; }
    protected virtual void Awake()
    {
        inCooldown = false;
        MaxLevel = 4;
        Level = 0;
        caster = transform.parent.gameObject;
    }
    protected virtual void Start()
    {
            
    }

    public virtual bool ActivateIndicator(GameObject pcaster)
    {
        if (Activatable && Unlocked)
        {
            caster = pcaster;

            if (currentRangeIndicator == null)
            {
                currentRangeIndicator = Instantiate(rangeIndicator, pcaster.transform.position, Quaternion.Euler(90, 0, 0), pcaster.transform);
                currentRangeIndicator.transform.localScale = new Vector3(CastRange, CastRange, 1);
            }

            return true;
        }
        else
            return false;
    }

    public void DeactivateIndicator()
    {
        if(currentRangeIndicator != null)
            Destroy(currentRangeIndicator);
    }

    public bool CanAffect(GameObject target)
    {
        if (target.GetComponent<Unit>() && targeting == Targeting.None)
            return true;
        else if (target.GetComponent<Unit>() == null)
            return false;

        foreach(Affects affect in affects)
        {
            switch (affect)
            {
                case Affects.Self:
                    if (target == caster)
                        return true;
                    break;

                case Affects.Enemy:
                    if (target.GetComponent<Unit>().faction != caster.GetComponent<Unit>().faction)
                        return true;
                    break;

                case Affects.EnemyHero:
                    if (target.GetComponent<Unit>().faction != caster.GetComponent<Unit>().faction
                        && target.CompareTag("Hero"))
                        return true;
                    break;

                case Affects.AllyHero:
                    if (target.GetComponent<Unit>().faction == caster.GetComponent<Unit>().faction
                        && target.CompareTag("Hero"))
                        return true;
                    break;

                case Affects.Ally:
                    if (target.GetComponent<Unit>().faction == caster.GetComponent<Unit>().faction)
                        return true;
                    break;

                default:
                    return false;
            }
        }

        return false;
    }

    public bool CanTarget(Vector3 targetPos, GameObject target = null)
    {
        switch (targeting)
        {
            case Targeting.Self:
                return (CanAffect(target)) ? true : false;
            case Targeting.Single:
                return (Vector3.Distance(caster.transform.position, target.transform.position) < CastRange && CanAffect(target)) ? true : false;
            default:
                return true;
        }
    }

    public virtual bool Cast(GameObject pCaster, Vector3 targetPos, GameObject ptarget = null)
    {
        ManaComponent manaComp = caster.GetComponent<ManaComponent>();
        caster = pCaster;
        target = ptarget;

        if (!Unlocked || !Activatable)
            return false;

        if (!CanTarget(targetPos, ptarget))
            return false;

        if (!CheckMana(pCaster, manaComp) || inCooldown)
            return false;

        manaComp.ReduceResource(ManaCost);
        return true;
    }

    public bool CheckMana(GameObject pCaster, ManaComponent manaComp)
    {
        if (manaComp.CurrentValue < ManaCost)
            return false;
        else
            return true;
    }

    protected virtual void OnProjectileHit(GameObject hit)
    {
        if (CanAffect(hit))
        {
            ApplyEffect(hit);
        }
    }

    protected virtual void ApplyEffect(GameObject target)
    {
        foreach(GameObject effect in effects)
        {
            GameObject statusEffect = Instantiate(effect);
            InitializeEffect(effect);
            statusEffect.GetComponent<StatusEffect>().ApplyEffect(target.GetComponent<Unit>());
        }
    }

    protected virtual void RemoveEffect(GameObject target)
    {
        foreach (GameObject effect in effects)
        {
            effect.GetComponent<StatusEffect>().RemoveEffect(target.GetComponent<Unit>());
        }
    }

    public virtual void InitializeEffect(GameObject effect)
    {

    }

    public virtual void LevelUp()
    {
        Level++;
    }
}
