﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float cameraSpeed;
    public float borderThreshold = 10f;

    [Header("Map Boundaries (x - vertical, y - horizontal)")]
    public Vector2 minBound;
    public Vector2 maxBound;

    [SerializeField] GameObject player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        if (Input.mousePosition.y >= Screen.height - borderThreshold)
            pos.x -= cameraSpeed * Time.unscaledDeltaTime;
        if (Input.mousePosition.y <= borderThreshold)
            pos.x += cameraSpeed * Time.unscaledDeltaTime;
        if (Input.mousePosition.x >= Screen.width - borderThreshold)
            pos.z += cameraSpeed * Time.unscaledDeltaTime;
        if (Input.mousePosition.x <= borderThreshold)
            pos.z -= cameraSpeed * Time.unscaledDeltaTime;

        pos.x = Mathf.Clamp(pos.x, minBound.x, maxBound.x);
        pos.z = Mathf.Clamp(pos.z, minBound.y, maxBound.y);
        transform.position = pos;

    }
}
