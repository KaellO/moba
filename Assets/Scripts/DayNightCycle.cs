﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayNightCycle : MonoBehaviour
{
    public float dayLength;
    public float startTime;
    public Vector3 noon;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float time;
    private float timeRate;

    [Header("Sun")]
    public Light sun;
    public Gradient sunColor;
    public int sunIntensity;

    [Header("Clock")]
    public float gameTime = 0;
    public Text gameTimeText;

    // Start is called before the first frame update
    void Start()
    {
        timeRate = 1.0f / dayLength;
        time = startTime;
        sun.intensity = 1;
    }

    // Update is called once per frame
    void Update()
    {
        time += timeRate * Time.deltaTime;
        gameTime += Time.deltaTime;

        if (time >= 1.0f)
        {
            sun.intensity = 1 - sun.intensity;
            time = 0.0f;
        }
        

        //sun.transform.eulerAngles = time * noon * 4.0f;

        int minutes = Mathf.FloorToInt(gameTime / 60f);
        int seconds = Mathf.FloorToInt(gameTime - minutes * 60);
        string formattedTime = string.Format("{0:00}:{1:00}", minutes, seconds);

        gameTimeText.text = formattedTime;
    }
}
