﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriticalStrikeSource
{
    public string ID;
    public float CriticalDamage;
    public float CriticalRate;

    public CriticalStrikeSource(string id, float cdmg, float crate)
    {
        ID = id;
        CriticalDamage = cdmg;
        CriticalRate = crate;
    }
}
