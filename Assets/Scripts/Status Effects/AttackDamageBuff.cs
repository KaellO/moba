﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDamageBuff : StatusEffect
{
    [SerializeField] Unit.AttackType attackType;
    public override bool ApplyEffect(Unit target)
    {
        if(target.attackType == attackType)
        {
            if (!base.ApplyEffect(target))
                return false;
            target.GetComponent<Stats>().damage += (int)power;
        }

        return true;
    }

    public override bool RemoveEffect(Unit target)
    {
        if (!base.RemoveEffect(target))
            return false;

        target.GetComponent<Stats>().damage -= (int)power;

        return true;
    }
}
