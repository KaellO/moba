﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StunEffect : StatusEffect
{
    public override bool ApplyEffect(Unit target)
    {
        Target = target;
        if (target == null)
            return false;

        target.statusEffects.Add(this);

        var worseStuns = target.statusEffects.Where(effect => effect.name == this.name && effect.currentTime > this.currentTime) as StatusEffect;
        target.statusEffects.Remove(worseStuns);

        StartCoroutine(handleTimer());

        if(target.TryGetComponent<Animator>(out Animator anim))
        {
            anim.SetTrigger("stun");
        }

        return true;
    }
    public override bool RemoveEffect(Unit target)
    {
        if (target.TryGetComponent<Animator>(out Animator anim))
        {
            anim.ResetTrigger("stun");
            anim.SetBool("idle", true);
        }
        base.RemoveEffect(target);

        return true;
    }
}
