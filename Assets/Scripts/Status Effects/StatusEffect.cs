﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StatusEffect : MonoBehaviour
{
    public enum Stacking
    {
        none,
        stacking,
        replace
    }
    public string Name;
    public float power;
    public float duration;
    public float currentTime = 0;
    public bool dispellable;
    public Stacking stacking;

    protected Unit Target;
    bool CheckStacking(Unit target)
    {
        switch (stacking)
        {
            case Stacking.none:
                if (target.statusEffects.Any(effect => effect.Name == Name))
                    return false;
                break;
            case Stacking.stacking:
                return true;
            case Stacking.replace:
                return true;
            default:
                return false;
        }
        return true;
    }
    public virtual bool ApplyEffect(Unit target)
    {
        Target = target;
        if (target == null)
            return false;

        if(!CheckStacking(target))
            return false;


        target.statusEffects.Add(this);
        return true;
    }

    public virtual bool RemoveEffect(Unit target)
    {
        if (target == null || !target.statusEffects.Contains(this))
            return false;

        target.statusEffects.Remove(this);
        Target = null;
        return true;
    }
    protected IEnumerator handleTimer()
    {
        currentTime = duration;
        while (currentTime >= 0)
        {
            currentTime -= Time.deltaTime;
            yield return null;
        }
        RemoveEffect(Target);
        Destroy(this.gameObject);
    }

}
