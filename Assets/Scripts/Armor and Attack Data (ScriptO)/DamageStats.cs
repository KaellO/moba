﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Damage Type", menuName = "DamageType")]
public class DamageStats : ScriptableObject
{
    public string Name;
    public float BasicArmor;
    public float Fortified;
    public float Hero;
}
