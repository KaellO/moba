﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Creep : Unit
{
    Stats stats;
    [SerializeField] int goldReward = 100;
    [SerializeField] int expReward = 100;
    [SerializeField] CreepScaling scalingValues;
    public override void Loot(GameObject lootReciever)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, lootRange, LayerMask.GetMask("Unit"), QueryTriggerInteraction.Ignore);

        int heroCount = hitColliders.Where(hero => hero.CompareTag("Hero") && hero.GetComponent<Unit>().faction != faction).Count();

        foreach(Collider col in hitColliders)
        {
            if(col.CompareTag("Hero"))
            {
                PlayerBase player = col.GetComponent<PlayerBase>();
                if (player.faction != faction)
                {
                    if (col.gameObject == lootReciever)
                    {
                        player.Gold += goldReward;

                        if (TryGetComponent(out FloatingTextSpawner textSpawner))
                        {
                            textSpawner.SpawnText(goldReward.ToString(), Color.yellow);
                        }
                    }
                    col.GetComponent<ExpComponent>().AddResource(expReward / heroCount);
                }
            }
        }
        Destroy(gameObject);
    }
    private void Awake()
    {
        stats = GetComponent<Stats>();
    }
    public enum CreepType
    {
        Melee,
        Ranged,
        Siege
    };
    public void ScaleUp()
    {
        if (scalingValues)
        {
            goldReward += scalingValues.GoldBountyGrowth;
            expReward += scalingValues.ExperienceBountyGrowth;
            stats.maxHealth += scalingValues.HealthGrowth;
            stats.damage += scalingValues.AttackDamageGrowth;
        }
    }
}
