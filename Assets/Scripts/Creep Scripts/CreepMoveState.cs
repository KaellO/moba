﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreepMoveState : UnitBaseFSM
{
    Movement movement;
    CreepAI creepAI;
    bool goNext = true;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        movement = unit.GetComponent<Movement>();
        creepAI = unit.GetComponent<CreepAI>();
        agent.isStopped = false;
        goNext = true;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (agent.remainingDistance <= 1 && goNext)
        {
            creepAI.GoToNextNode();
            goNext = false;
        }

        if (movement.turnAngle > 11)
        {
            animator.SetBool("turning", true);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("moving", false);
        animator.SetFloat("moveAnimOffset", (stateInfo.normalizedTime + animator.GetFloat("moveAnimOffset")) % 1);
    }
}
