﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class CreepSpawner : MonoBehaviour
{
    GameObject[] allNodes;
    List<GameObject> laneNodes = new List<GameObject>();
    [SerializeField] List<GameObject> StandardCreepWave;
    [SerializeField] GameObject siegeCreep;

    [SerializeField] GameObject superMelee;
    [SerializeField] GameObject superRanged;
    [SerializeField] GameObject superSiege;

    [SerializeField] GameObject enemyAncient;

    GameObject firstNode;
    Image miniMap;

    float IndividualSpawnInterval = 0.5f;

    int WaveCount = 0;
    int BarracksDestroyed = 0;
    bool spawning = false;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CallSpawnWave", 30f, 30f);
        StartCoroutine(CreepScaling());
        allNodes = GameObject.FindGameObjectsWithTag("Node");
        miniMap = GameObject.FindGameObjectWithTag("Minimap").GetComponent<Image>();

        float smallestDist = Vector3.Distance(transform.position, allNodes[0].transform.position);
        firstNode = allNodes[0];
        foreach (GameObject node in allNodes)
        {
            if (smallestDist > Vector3.Distance(transform.position, node.transform.position))
            {
                smallestDist = Vector3.Distance(transform.position, node.transform.position);
                firstNode = node;
            }
        }

        GetLaneNodes(firstNode);
    }

    IEnumerator CreepScaling()
    {
        while (true)
        {
            yield return new WaitForSeconds(180f); 
            foreach(GameObject creep in StandardCreepWave)
            {
                creep.GetComponent<Creep>().ScaleUp();
            }
        }
    }

    //Gets the lane path of the node
    //Not very optimized O(n) but it should not be called very often.
    void GetLaneNodes(GameObject nearestNode)
    {
        //Current node is the start 
        GameObject currentNode = nearestNode;
        List<GameObject> evalNodes = allNodes.ToList<GameObject>();

        evalNodes.Remove(currentNode);
        laneNodes.Add(currentNode);

        while(evalNodes.Count > 0)
        {
            float smallestDist = float.PositiveInfinity;
            float distToAncient = Vector3.Distance(currentNode.transform.position, enemyAncient.transform.position);
            GameObject nextNode = null;
            foreach (GameObject node in evalNodes.ToList())
            {
                //if the node is further from the ancient than the current node then remove it.
                if (Vector3.Distance(node.transform.position, enemyAncient.transform.position) >= distToAncient)
                {
                    evalNodes.Remove(node);
                    continue;
                }
                //grab the next node by getting the nearest node.
                if (Vector3.Distance(node.transform.position, currentNode.transform.position) < smallestDist)
                {
                    smallestDist = Vector3.Distance(node.transform.position, currentNode.transform.position);
                    nextNode = node;
                }
            }
            //set the nearest node and evaluate that next.
            currentNode = nextNode;

            if(currentNode)
            {
                laneNodes.Add(currentNode);
                evalNodes.Remove(currentNode);
            }
        }
    }
    void SpawnCreep(GameObject singleCreep)
    {
        GameObject creep = Instantiate(singleCreep, transform.position, transform.rotation);
        CreepAI creepAI = creep.GetComponent<CreepAI>();

        creepAI.GetComponent<MinimapIcon>().MinimapImage = miniMap;
        creepAI.allNodes = allNodes;
        creepAI.enemyAncient = enemyAncient;
        creepAI.currentNode = firstNode;
        creepAI.laneNodes = laneNodes;
    }
    void CallSpawnWave()
    {
        StartCoroutine(SpawnWave());
        WaveCount++;
    }

    //hard coded oof
    public void OnEnemyMeleeDestroyed()
    {
        StandardCreepWave.RemoveRange(0, 3);
        for (int i = 0; i < 3; i++) { StandardCreepWave.Insert(0, superMelee); }
        BarracksDestroyed++;
    }

    public void OnEnemyRangedDestroyed()
    {
        StandardCreepWave.RemoveAt(StandardCreepWave.Count - 1);
        StandardCreepWave.Add(superRanged);
        BarracksDestroyed++;
    }
    //fix bug where it stops spawning when barracks are destroyed.
    IEnumerator SpawnWave()
    {
        spawning = true;
        foreach (GameObject creep in StandardCreepWave)
        {
            SpawnCreep(creep);
            yield return new WaitForSeconds(IndividualSpawnInterval);
        }

        if (WaveCount % 5 == 0 && siegeCreep)
        {
            if (BarracksDestroyed == 2)
                SpawnCreep(superSiege);
            else
                SpawnCreep(siegeCreep);
        }
        spawning = false;
    }
}
