﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CreepAI : MonoBehaviour
{
    Movement movementComp;
    Attack attackComp;
    Animator animator;
    Creep unitData;
    HealthComponent healthComp;
    AutoTargetComponent targetComp;

    public GameObject enemyAncient;
    public GameObject[] allNodes;
    public List<GameObject> laneNodes;
    public LayerMask unitMask;

    public GameObject currentNode;

    int curretNodeIndex;

    void Start()
    {
        healthComp = GetComponent<HealthComponent>();
        movementComp = GetComponent<Movement>();
        attackComp = GetComponent<Attack>();
        animator = GetComponent<Animator>();
        unitData = GetComponent<Creep>();
        targetComp = GetComponent<AutoTargetComponent>();

        curretNodeIndex = 0;

        targetComp.OnFirstTargetFound += FirstTargetFound;
        targetComp.OnTargetLeft += TargetLeftRadius;

        movementComp.SetTargetNode(currentNode.transform.position);
        animator.SetBool("turning", true);

        animator.SetFloat("attackSpeed", 1);
    }

    // Update is called once per frame
    void Update()
    {
        //Look for next target and reset state if current target dies
        if (targetComp.targets.Count > 0 && attackComp.currentTarget == null)
        {
            animator.SetBool("idle", true);

            LookForNextTarget();
        }
    }

    public GameObject GetClosestNode()
    {
        GameObject closestNode = currentNode;
        float smallestDist = float.PositiveInfinity;
        float distToAncient = Vector3.Distance(transform.position, enemyAncient.transform.position);

        foreach (GameObject node in allNodes)
        {
            //if the node is further from the ancient than the current node then skip.
            if (Vector3.Distance(transform.position, enemyAncient.transform.position) >= distToAncient)
            {
                continue;
            }
            //grab the next node by getting the nearest node.
            if (Vector3.Distance(transform.position, node.transform.position) < smallestDist)
            {
                smallestDist = Vector3.Distance(transform.position, node.transform.position);
                closestNode = node;
            }
        }
        return closestNode;
    }

    //Resets current node and sets a new lane.
    public void FindLane()
    {
        curretNodeIndex = 0;
        laneNodes.Clear();

        //Evaluate the Lane
        //Current node is the start 
        GameObject firstNode = GetClosestNode();
        List<GameObject> evalNodes = allNodes.ToList<GameObject>();

        while (evalNodes.Count > 0)
        {
            float smallestDist = float.PositiveInfinity;
            float distToAncient = Vector3.Distance(firstNode.transform.position, enemyAncient.transform.position);
            GameObject nextNode = null;
            foreach (GameObject node in evalNodes.ToList())
            {
                //if the node is further from the ancient than the current node then remove it.
                if (Vector3.Distance(node.transform.position, enemyAncient.transform.position) >= distToAncient)
                {
                    evalNodes.Remove(node);
                    continue;
                }
                //grab the next node by getting the nearest node.
                if (Vector3.Distance(node.transform.position, firstNode.transform.position) < smallestDist)
                {
                    smallestDist = Vector3.Distance(node.transform.position, firstNode.transform.position);
                    nextNode = node;
                }
            }
            //set the nearest node and evaluate that next.
            firstNode = nextNode;

            if (firstNode)
            {
                laneNodes.Add(firstNode);
                evalNodes.Remove(firstNode);
            }

            currentNode = laneNodes[0];
            movementComp.SetTargetNode(currentNode.transform.position);
        }
    }

    public void GoToNextNode()
    {
        if (curretNodeIndex >= laneNodes.Count - 1)
            return;

        curretNodeIndex++;
        currentNode = laneNodes[curretNodeIndex];
        movementComp.SetTargetNode(currentNode.transform.position);
    }

    void FirstTargetFound(GameObject other)
    {
        if(other)
        {
            attackComp.SetTarget(other.gameObject);
            movementComp.SetTargetNode(other.gameObject.transform.position);
            animator.SetBool("turning", true);
        }
    }

    public void LookForNextTarget()
    {
        if (targetComp.targets.Count <= 0)
        {
            attackComp.currentTarget = null;

            //just stop if no target, idle state will handle it
            animator.SetBool("idle", true);
        }

        if (targetComp.targets.Count > 0)
        {
            targetComp.targets.OrderBy(enemy => Vector3.Distance(transform.position, enemy.transform.position));
            attackComp.SetTarget(targetComp.targets[0]);
        }

        animator.SetBool("turning", true);
    }

    void TargetLeftRadius(GameObject target)
    {
        if (attackComp.currentTarget == target)
        {
            attackComp.currentTarget = null;
            LookForNextTarget();
        }
    }
}
