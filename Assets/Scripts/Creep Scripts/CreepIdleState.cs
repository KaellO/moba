﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreepIdleState : UnitBaseFSM
{
    Movement movement;
    CreepAI creepAI;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        movement = unit.GetComponent<Movement>();
        creepAI = unit.GetComponent<CreepAI>();

        if (creepAI.currentNode != creepAI.GetClosestNode())
        {
            creepAI.FindLane();
        }
        else
        {
            movement.SetTargetNode(creepAI.currentNode.transform.position);
            animator.SetBool("turning", true);
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
