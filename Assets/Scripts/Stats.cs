﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Stats : MonoBehaviour
{
    public float baseSpeed;
    public float ExSpeed { get; private set; }
    public float Speed
    {
        get { return baseSpeed; }
        set
        {
            ExSpeed += Mathf.Abs(baseSpeed - value);
            baseSpeed = Mathf.Clamp(value, 1, 5.5f);
            GetComponent<NavMeshAgent>().speed = baseSpeed;
        }
    }

    public int maxHealth;
    public float healthRegen;
    public int maxMana;
    public float manaRegen;

    [Header("Armor Properties")]
    public int armor;
    [Range(0,1)] public float baseMagicResist;
    public enum ArmorType
    {
        Basic,
        Fortified,
        Hero
    };

    public ArmorType armorType;
    [HideInInspector] public List<float> magicResist { get; private set; }
    [HideInInspector] public List<float> magicReduction { get; private set; }
    [HideInInspector] public List<float> slows { get; private set; }
    [HideInInspector] public List<CriticalStrikeSource> critSources { get; private set; }
    [HideInInspector] public Dictionary<string, float> evasion { get; private set; }
    [Header("Damage Properties")]
    public float damage;
    public enum DamageType
    {
        Basic,
        Pierce, 
        Siege,
        Hero
    };

    public DamageType damageType;
    [SerializeField] private float attackSpeed;
    public float AttackSpeed   
    {
        get { return attackSpeed; }
        set
        {
            attackSpeed = Mathf.Clamp(value, 20, 700);
        }
    }
    public float BAT = 1f;
    public float attackRange;

    public float CalculateAttackTime()
    {
        float attacksPerSecond = AttackSpeed / (100 * BAT);
        float attackTime = 1 / attacksPerSecond;

        return attackTime;
    }

    private void Awake()
    {
        magicResist = new List<float>();
        magicReduction = new List<float>();
        slows = new List<float>();
        evasion = new Dictionary<string, float>();
        critSources = new List<CriticalStrikeSource>();
        magicResist.Add(baseMagicResist);
    }
}
