﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBarUI : MonoBehaviour
{
    public GameObject resourceBase;
    public Image resourceBar;
    public TextMeshProUGUI resourceText;
    public Resource resourceComponent;
    public TextMeshProUGUI regenText;

    public bool WorldSpace;

    private void Awake()
    {
        if(resourceComponent)
        {
            resourceComponent.OnUpdateResource += UpdateHealthBar;
        }
    }

    public void ChangeHealthComponent(Resource resourceC)
    {
        resourceBase.SetActive(resourceC != null);
        if (resourceC == null)
            return;

        if (resourceComponent)
        {
            resourceComponent.OnUpdateResource -= UpdateHealthBar;
        }

        resourceComponent = resourceC;
        resourceComponent.OnUpdateResource += UpdateHealthBar;
        resourceComponent.Initialize();
    }
    public void UpdateHealthBar(float hp, float maxHp)
    {

        resourceBar.fillAmount = hp / maxHp;

        if(resourceText)
            resourceText.text = hp.ToString() + "/" + maxHp.ToString();
        if(regenText)
        {
            regenText.enabled = hp != maxHp;
            regenText.text = "+" + resourceComponent.RegenValue.ToString();
        }
    }

    private void Update()
    {
        if(WorldSpace)
        {
            Vector3 lookDir = transform.position - Camera.main.gameObject.transform.position;
            lookDir.z = 0;

            transform.rotation = Quaternion.LookRotation(lookDir, Vector3.up);
        }
    }
}
