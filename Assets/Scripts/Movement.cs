﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour
{
    [SerializeField] bool ShowDebugLine;
    NavMeshAgent agent;

    //Determine turning
    [HideInInspector] public Vector3 rotTargetLocation;
    public float rotationSpeed;

    LineRenderer line;

    [HideInInspector] public Vector3[] pathCorners;
    [HideInInspector] public int currentCorner;
    [HideInInspector] public float turnAngle;
    [HideInInspector] public Vector3 targetDir;

    //For Debugging only
    [SerializeField] Vector3 targetNode;


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();

        //if (ShowDebugLine)
        //    line = GetComponent<LineRenderer>();         
    }

    void Start()
    {
        rotationSpeed = 1440 * Mathf.Deg2Rad;

        turnAngle = 0;
        targetDir = Vector3.zero;
    }
    private void Update()
    {
        if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.hasPath)
        {
            //Set next corner in path for rotation if you reach the node
            if (Mathf.Abs(Vector3.Distance(transform.position, rotTargetLocation)) < 1)
            {
                if (currentCorner < pathCorners.Length)
                    rotTargetLocation = pathCorners[currentCorner++];

            }
            targetDir = rotTargetLocation - transform.position;
            turnAngle = Vector3.Angle(targetDir, transform.forward);
        }

        //Turn without triggering state if below threshold 
        if (turnAngle < 11)
        {
            Vector3 cornerDirection = Vector3.RotateTowards(transform.forward, targetDir, Time.deltaTime * rotationSpeed, 0.0f);
            transform.rotation = Quaternion.LookRotation(cornerDirection);
        }
    }

    public void SetTargetNode(Vector3 location)
    {
        targetNode = location;
        agent.ResetPath();
        currentCorner = 0;

        Vector3 endPoint = new Vector3(location.x, transform.position.y, location.z);

        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(endPoint, path);

        if (path.status == NavMeshPathStatus.PathInvalid)
        {
            endPoint = ReturnClosestValidPoint(endPoint);
            agent.CalculatePath(endPoint, path);
        }

        if (path.status != NavMeshPathStatus.PathInvalid)
        {
            pathCorners = path.corners;
            rotTargetLocation = new Vector3(pathCorners[currentCorner].x, transform.position.y, pathCorners[currentCorner].z);

            //if(ShowDebugLine)
            //{
            //    line.SetPosition(0, transform.position);
            //    DrawPath(path);
            //}
            
            agent.SetDestination(endPoint);
        }
    }

    public void UpdateLocation(Vector3 Location)
    {
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(Location, path);

        if (path.status != NavMeshPathStatus.PathInvalid)
        {
            //Works for now but its jittery
            currentCorner = 1;
            pathCorners = path.corners;
            rotTargetLocation = new Vector3(pathCorners[currentCorner].x, transform.position.y, pathCorners[currentCorner].z);

            agent.SetDestination(Location);
        }
    }
    public Vector3 ReturnClosestValidPoint(Vector3 point)
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(point, out hit, 20.0f, NavMesh.AllAreas))
        {
            return new Vector3 (hit.position.x, transform.position.y, hit.position.z);
        }

        return Vector3.zero;
    }
    void DrawPath(NavMeshPath path)
    {
        line.enabled = true;

        if (path.corners.Length < 2) 
            return;

        line.positionCount = path.corners.Length; 

        line.SetPositions(path.corners);
    }
}
