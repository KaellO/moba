﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimapIcon : MonoBehaviour
{
    [Header("Minimap Graphics")]
    public Image MinimapImage;
    public GameObject MinimapIconImage;

    [Header("Minimap Attributes")]
    public bool dynamicObject;
    //Make sure the pivot of the minimap is at the top left
    private float mapSize = 150f;
    private float sceneSize = 150f;

    private GameObject minimapIcon;
    Unit unit;
    // Start is called before the first frame update
    void Start()
    {
        if(!MinimapImage)
            MinimapImage = GameObject.FindGameObjectWithTag("Minimap").GetComponent<Image>();

        minimapIcon = Instantiate(MinimapIconImage, MinimapImage.transform);
        minimapIcon.transform.localPosition = new Vector3(GetMapPos(transform.position.z, mapSize, sceneSize), GetMapPos(transform.position.x * -1, mapSize, sceneSize), 0);
        //MinimapIconImage.transform.position = transform.position;      
        
        Image iconImage = minimapIcon.GetComponent<Image>();
        unit = GetComponent<Unit>();
        unit.GetComponent<HealthComponent>().OnUnitDeath.AddListener((go) => minimapIcon.SetActive(false));
        unit.OnRespawn += () => minimapIcon.SetActive(true);
        switch (unit.faction)
        {
            case Unit.Faction.Dire:
                iconImage.color = Color.red;
                break;
            case Unit.Faction.Radiant:
                iconImage.color = Color.green;
                break;
        }
        
    }

    float GetMapPos(float pos, float mapSize, float sceneSize)
    {
        return pos * mapSize / sceneSize;
    }

    // Update is called once per frame
    void Update()
    {
        if(dynamicObject)
            minimapIcon.transform.localPosition = new Vector3(GetMapPos(transform.position.z, mapSize, sceneSize), GetMapPos(transform.position.x * -1, mapSize, sceneSize), 0);
    }
}
