﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootProjectile : MonoBehaviour
{
    [SerializeField] GameObject Projectile;
    [SerializeField] public Transform shootPoint;

    public GameObject altTarget;

    public GameObject projectileSpawned;
    [HideInInspector] public Attack attackComp;
    void Start()
    {
        attackComp = GetComponent<Attack>();

        //if (!shootPoint)
        //{
        //    shootPoint = GameObject.Find("ShootPoint").transform;
        //    if (!shootPoint)
        //        Debug.LogError("Missing ShootPoint on " + gameObject.name);
        //}
    }

    public GameObject SingleTargetProjectile()
    {
        projectileSpawned = Instantiate(Projectile, shootPoint.position, transform.rotation);
        projectileSpawned.transform.position = shootPoint.position;
        Projectile projectileProp = projectileSpawned.GetComponent<Projectile>();

        //DEBUG ONLY
        projectileProp.caster = this.gameObject;
        if(projectileProp)
        {
            if (attackComp.currentTarget != null)
                projectileProp.target = attackComp.currentTarget;
            else if (altTarget != null)
                projectileProp.target = altTarget;

            projectileProp.OnHit += DestroyOnCollide;

            return projectileSpawned;
        }
        return null;
    }
    
    void DestroyOnCollide(GameObject hit)
    {
        if (!attackComp || !hit)
            return;

        if (hit == attackComp.currentTarget)
        {
            attackComp.AutoAttack(hit);
            Destroy(projectileSpawned);
        }
    }

    public GameObject NoTargetProjectile(Vector3 dir)
    {
        GameObject projectile = Instantiate(Projectile, shootPoint.position, Quaternion.identity);
        projectile.transform.position = shootPoint.position;
        Vector3 direction = new Vector3(dir.x, projectile.transform.position.y, dir.z);
        projectile.transform.LookAt(direction);

        return projectile;
    }
}
