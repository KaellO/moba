﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Projectile : MonoBehaviour
{
    [HideInInspector] public event Action<GameObject> OnHit;
    public GameObject target;
    public float maxDist;
    public float speed;

    //NOTE: FOR DEBUG ONLY
    public GameObject caster;
    [SerializeField] Vector3 originalTransform;

    private void Start()
    {
        originalTransform = transform.position;   
    }

    private void Update()
    {
        if (target)
        {
            Vector3 targetPos = target.transform.position;

            Vector3 dir = targetPos - transform.position;
            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

            Vector3 resultingDirection = Vector3.RotateTowards(transform.forward, dir, 9999 * Mathf.Deg2Rad * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(resultingDirection);
        }

        if (Vector3.Distance(transform.position, originalTransform) <= maxDist && !target)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
        else if (!target)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        OnHit?.Invoke(other.gameObject);
    }
}
