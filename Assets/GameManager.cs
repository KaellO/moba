﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject winPanel;
    public TextMeshProUGUI WinText;

    public void WinGame(GameObject winner)
    {
        Unit.Faction winnerFaction = winner.GetComponent<Unit>().faction;

        switch (winnerFaction)
        {
            case Unit.Faction.Dire:
                WinText.color = Color.green;
                winnerFaction = Unit.Faction.Radiant;
            break;

            case Unit.Faction.Radiant:
                WinText.color = Color.red;
                winnerFaction = Unit.Faction.Dire;
            break;
        }

        string winnerText = winnerFaction.ToString();
        winnerText.ToUpper();
        WinText.text = winnerText + " VICTORY!";
        
        winPanel.SetActive(true);

        Time.timeScale = 0;
    }

    public void ChooseNewHero()
    {
        SceneManager.LoadScene("HeroSelect");
    }
}
