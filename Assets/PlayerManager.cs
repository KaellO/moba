﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance { get; private set; }
    public Transform[] spawnPoints;
    public GameObject[] players { get; private set; }
    public int RadiantKills { get; private set; }
    public int DireKills { get; private set; }
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        players = GameObject.FindGameObjectsWithTag("Hero");

        foreach (GameObject player in players)
        {
            player.GetComponent<PlayerBase>().OnHeroKilled += UpdateKillCount;
        }
    }

    void UpdateKillCount(GameObject killer)
    {
        if (killer.GetComponent<Unit>().faction == Unit.Faction.Radiant)
        {
            RadiantKills++;
        }
        if (killer.GetComponent<Unit>().faction == Unit.Faction.Dire)
        {
            DireKills++;
        }
    }
}
