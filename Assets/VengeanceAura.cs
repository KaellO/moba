﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VengeanceAura : Skills
{
    SphereCollider sCollider;
    [Header("ScalingValues")]
    public float[] rangeBonus;
    public float[] damageBonus;

    protected override void Start()
    {
        base.Start();
        inCooldown = false;
        sCollider = GetComponent<SphereCollider>();
        sCollider.radius = CastRange;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger)
            return;

        if (CanAffect(other.gameObject))
        {
            ApplyEffect(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.isTrigger)
            return;

        RemoveEffect(other.gameObject);
    }

    public override void InitializeEffect(GameObject effect)
    {
        if (effect.TryGetComponent(out AttackRangeBuff attackRangeBuff))
            attackRangeBuff.power = rangeBonus[Level - 1];

        if (effect.TryGetComponent(out AttackDamageBuff attackDamageBuff))
            attackDamageBuff.power = damageBonus[Level - 1];
    }
}
