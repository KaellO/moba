﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class HeroAI : MonoBehaviour
{
    GameObject[] allNodes;
    List<GameObject> laneNodes = new List<GameObject>();

    [SerializeField] GameObject enemyAncient;

    [SerializeField] GameObject[] startLaneNodes;
    GameObject firstNode;
    private void Awake()
    {
        allNodes = GameObject.FindGameObjectsWithTag("Node");

        float smallestDist = Vector3.Distance(transform.position, allNodes[0].transform.position);
        firstNode = startLaneNodes[Random.Range(0, startLaneNodes.Length)];
        GetLaneNodes(firstNode);

        InitializeAI(this.gameObject);
    }
    void GetLaneNodes(GameObject nearestNode)
    {
        //Current node is the start 
        GameObject currentNode = nearestNode;
        List<GameObject> evalNodes = allNodes.ToList<GameObject>();

        evalNodes.Remove(currentNode);
        laneNodes.Add(currentNode);

        while (evalNodes.Count > 0)
        {
            float smallestDist = float.PositiveInfinity;
            float distToAncient = Vector3.Distance(currentNode.transform.position, enemyAncient.transform.position);
            GameObject nextNode = null;
            foreach (GameObject node in evalNodes.ToList())
            {
                //if the node is further from the ancient than the current node then remove it.
                if (Vector3.Distance(node.transform.position, enemyAncient.transform.position) >= distToAncient)
                {
                    evalNodes.Remove(node);
                    continue;
                }
                //grab the next node by getting the nearest node.
                if (Vector3.Distance(node.transform.position, currentNode.transform.position) < smallestDist)
                {
                    smallestDist = Vector3.Distance(node.transform.position, currentNode.transform.position);
                    nextNode = node;
                }
            }
            //set the nearest node and evaluate that next.
            currentNode = nextNode;

            if (currentNode)
            {
                laneNodes.Add(currentNode);
                evalNodes.Remove(currentNode);
            }
        }
    }

    public void InitializeAI(GameObject phero)
    {
        CreepAI creepAI = GetComponent<CreepAI>();

        creepAI.allNodes = allNodes;
        creepAI.enemyAncient = enemyAncient;
        creepAI.currentNode = firstNode;
        creepAI.laneNodes = laneNodes;
    }
}
