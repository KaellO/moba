﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TopUI : MonoBehaviour
{
    [Header("Radiant Side")]
    [SerializeField] GameObject rHeroesPanel;
    [SerializeField] TextMeshProUGUI rKills;
    List<GameObject> rHeroPanels = new List<GameObject>();

    [Header("Dire Side")]
    [SerializeField] GameObject dHeroesPanel;
    [SerializeField] TextMeshProUGUI dKills;
    [SerializeField] GameObject PortraitPrefab;
    List<GameObject> dHeroPanels = new List<GameObject>();

    private void Start()
    {
        GameObject[] players = PlayerManager.instance.players;
        foreach (GameObject player in players)
        {
            //GameObject NewObj = new GameObject(); 
            //Image NewImage = NewObj.AddComponent<Image>();
            //NewImage.sprite = player.GetComponent<Unit>().portrait; 

            //if (player.GetComponent<Unit>().faction == Unit.Faction.Radiant)
            //{
            //    NewObj.GetComponent<RectTransform>().SetParent(rHeroesPanel.transform);
            //}
            //else if (player.GetComponent<Unit>().faction == Unit.Faction.Dire)
            //{
            //    NewObj.GetComponent<RectTransform>().SetParent(dHeroesPanel.transform);
            //}

            //NewImage.rectTransform.sizeDelta = new Vector2(50, 50);
            //NewObj.SetActive(true);
            GameObject go = Instantiate(PortraitPrefab);
            Image image = go.GetComponent<Image>();
            Unit unit = player.GetComponent<Unit>();
            image.sprite = unit.portrait;

            if (unit.faction == Unit.Faction.Radiant)
            {
                go.GetComponent<RectTransform>().SetParent(rHeroesPanel.transform, false);
            }
            else if (unit.faction == Unit.Faction.Dire)
            {
                go.GetComponent<RectTransform>().SetParent(dHeroesPanel.transform, false);
            }

            player.GetComponent<PlayerBase>().OnHeroKilled += UpdateKillCount;
            image.rectTransform.sizeDelta = new Vector2(50, 50);
            player.GetComponent<PlayerBase>().OnResTimerUpdate += go.GetComponent<RespawnUI>().Respawning;
            unit.OnRespawn += go.GetComponent<RespawnUI>().Respawned;
        }
    }
    public void UpdateKillCount(GameObject killer)
    {
        dKills.text = PlayerManager.instance.DireKills.ToString();
        rKills.text = PlayerManager.instance.RadiantKills.ToString();
    }
}
