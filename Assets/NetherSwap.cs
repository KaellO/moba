﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetherSwap : Skills
{
    [Header("ScalingValues")]
    public float[] CastRangeScaling;
    public float[] ManaCostScaling;
    protected override void Awake()
    {
        MaxLevel = 3;
    }
    protected void Start()
    {
        inCooldown = false;
    }

    public override bool Cast(GameObject pCaster, Vector3 targetPos, GameObject target = null)
    {
        if (!base.Cast(pCaster, targetPos, target))
            return false;

        pCaster.GetComponent<Animator>().SetBool("idle", true);
        target.GetComponent<Animator>().SetBool("idle", true);

        Vector3 oldTransform = caster.transform.position;
        caster.transform.position = target.transform.position;
        target.transform.position = oldTransform;

        return true;
    }
    public override void LevelUp()
    {
        base.LevelUp();
        CastRange = CastRangeScaling[Level - 1];
        ManaCost = ManaCostScaling[Level - 1];
    }
}
