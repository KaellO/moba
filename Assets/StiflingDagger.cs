﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StiflingDagger : Skills
{
    GameObject projectile;
    [Header("Scaling Values")]
    public float[] DamagePrecentScaling;
    public float Slow;
    public float[] SlowDurationScaling;

    protected override void Start()
    {
        base.Start();
        inCooldown = false;
    }

    public override bool Cast(GameObject pCaster, Vector3 targetPos, GameObject target = null)
    {
        if (!base.Cast(pCaster, targetPos, target))
            return false;

        //Damage
        Damage = caster.GetComponent<Stats>().damage * ( 1 - DamagePrecentScaling[Level - 1]);
        

        shootProjectile.altTarget = target;
        shootProjectile.shootPoint = caster.transform;
        shootProjectile.shootPoint.position = new Vector3(caster.transform.position.x, caster.transform.position.y + 3, caster.transform.position.z);
        shootProjectile.attackComp = pCaster.GetComponent<Attack>();

        projectile = shootProjectile.SingleTargetProjectile();
        Projectile projectileComp = projectile.GetComponent<Projectile>();

        projectileComp.OnHit += OnProjectileHit;
        return true;
    }

    protected override void OnProjectileHit(GameObject hit)
    {
        if (CanAffect(hit))
        {
            ApplyEffect(hit);

            if (caster && hit)
                caster.GetComponent<Attack>().DamageTarget(hit, Damage, damageType);

            Destroy(projectile);
        }
    }

    public override void InitializeEffect(GameObject effect)
    {
        effect.GetComponent<SlowEffect>().duration = SlowDurationScaling[Level - 1];
        effect.GetComponent<SlowEffect>().power = caster.GetComponent<Stats>().Speed * 0.5f;
    }

    public override void LevelUp()
    {
        base.LevelUp();
    }
}
