﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreboardUI : MonoBehaviour
{
    [SerializeField] GameObject KDAPrefab;
    [SerializeField] GameObject rHeroesPanel;
    [SerializeField] GameObject dHeroesPanel;
    Vector3 originalPosition;
    private void Start()
    {
        originalPosition = transform.position;
        GameObject[] players = PlayerManager.instance.players;
        foreach (GameObject player in players)
        {
            GameObject go = Instantiate(KDAPrefab);
            Unit unit = player.GetComponent<Unit>();
            KDAPanelUI kdaPanel = go.GetComponent<KDAPanelUI>();
            kdaPanel.nameText.text = unit.name;
            kdaPanel.portrait.sprite = unit.portrait;

            if (unit.faction == Unit.Faction.Radiant)
            {
                //go.GetComponent<RectTransform>().SetParent(rHeroesPanel.transform);
                go.GetComponent<RectTransform>().SetParent(rHeroesPanel.transform, false);
            }
            else if (unit.faction == Unit.Faction.Dire)
            {
                go.GetComponent<RectTransform>().SetParent(dHeroesPanel.transform, false);
            }

            player.GetComponent<PlayerBase>().UpdateKillCount += kdaPanel.UpdateKills;
            player.GetComponent<PlayerBase>().UpdateDeathCount += kdaPanel.UpdateDeaths;
            player.GetComponent<PlayerBase>().OnGoldRecieved += kdaPanel.UpdateGold;
        }
        transform.position = new Vector3(0, 5000, 0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
            transform.position = originalPosition;

        if (Input.GetKeyUp(KeyCode.BackQuote))
            transform.position = new Vector3(0, 5000, 0);
    }
}
