﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class KDAPanelUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI killsText;
    [SerializeField] TextMeshProUGUI deathsText;
    [SerializeField] TextMeshProUGUI goldText;
    public TextMeshProUGUI nameText;
    public Image portrait;

    public void UpdateKills(int kills)
    {
        killsText.text = kills.ToString();
    }

    public void UpdateDeaths(int deaths)
    {
        deathsText.text = deaths.ToString();
    }
    public void UpdateGold(int gold)
    {
        goldText.text = gold.ToString();
    }
}
