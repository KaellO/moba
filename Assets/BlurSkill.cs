﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlurSkill : Skills
{
    [Header("ScalingValues")]

    public float[] evasion;
    public float[] attackSpeed;
    public override void LevelUp()
    {
        base.LevelUp();
        Stats stats = caster.GetComponent<Stats>();
        if (stats.evasion.ContainsKey("Blur"))
        {
            stats.evasion.Remove("Blur");
            stats.evasion.Add("Blur", evasion[Level - 1]);
        }
        else
        {
            stats.evasion.Add("Blur", evasion[Level - 1]);
        }
    }
}

