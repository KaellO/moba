﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CoupDeGrace : Skills
{
    [Header("ScalingValues")]

    public float[] critDamage;
    public float critChance;
    protected override void Awake()
    {
        base.Awake();
        MaxLevel = 3;
    }
    public override void LevelUp()
    {
        base.LevelUp();
        Stats stats = caster.GetComponent<Stats>();

        foreach (CriticalStrikeSource source in stats.critSources)
        {
            if (source.ID == "CoupDeGrace")
            {
                stats.critSources.Remove(source);
                stats.critSources.Add(new CriticalStrikeSource("CoupDeGrace", critDamage[Level - 1], critChance));
                return;
            }
        }
        stats.critSources.Add(new CriticalStrikeSource("CoupDeGrace", critDamage[Level - 1], critChance));
    }
}

