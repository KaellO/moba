﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroSelection : MonoBehaviour
{
    public void SetHero(HeroData hero)
    {
        HeroSelectionData.selectedHero = hero;
        SceneManager.LoadScene("SampleScene");
    }
}
