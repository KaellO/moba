﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerSpecificUI : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] TextMeshProUGUI goldText;
    void Start()
    {
        player.GetComponent<PlayerBase>().OnGoldRecieved += UpdateGold;
    }

    void UpdateGold(int gold)
    {
        goldText.text = gold.ToString();
    }
}
