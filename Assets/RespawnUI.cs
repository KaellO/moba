﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class RespawnUI : MonoBehaviour
{
    [SerializeField] GameObject respawnPanel;
    [SerializeField] TextMeshProUGUI respawnText;
    [SerializeField] Image portrait;

    public void Respawning(int text)
    {
        respawnPanel.SetActive(true);
        respawnText.text = text.ToString();
        portrait.color = new Color(0.2f, 0.2f, 0.2f);
    }

    public void Respawned()
    {
        respawnPanel.SetActive(false);
        portrait.color = Color.white;
    }
}
